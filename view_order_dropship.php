<!DOCTYPE html>
	<?php
session_start();
if($_SESSION['email']=="")
{
	header("location: index.html");
}
else
{
	include("inc_db.php");
	if($DBconnect!==FALSE);
	{
		$SQLquery="SELECT * FROM user WHERE email='$_SESSION[email]'";
		$QueryResult=mysqli_query($DBconnect,$SQLquery);
		
		while($row=mysqli_fetch_array($QueryResult))
		{
			$id=$row["no_id"];
			$email=$_SESSION['email'];
?>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | LANEW Management System</title>
    <!-- Favicon-->
    <link rel="icon" href="lanew_icon.jpeg" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="css/themes/all-themes.css" rel="stylesheet" />
		
</head>


<body class="theme-amber">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">LANEW</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <?php 
					
						$SQLquery6="SELECT * FROM verify_user WHERE sponsor_id='$id'";
						$QueryResult6=mysqli_query($DBconnect,$SQLquery6);
							
						while($row6=mysqli_fetch_array($QueryResult6))
						{
							if($row6['dp_image']=="")
							{
							?>
								<img src="images/user.png" width="48" height="48" alt="User" />
							<?php
							}
							else
							echo '<img src="data:image/jpeg;base64,'.base64_encode( $row6['dp_image'] ).'" width="48" height="48"/>';
						}
					?>
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo "$id"; ?></div>
                    <div class="email"><?php echo "$email"; ?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="profile.php"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="view_sponsor.php"><i class="material-icons">group</i>Sponsor</a></li>
                            <li><a href="view_order_list.php"><i class="material-icons">shopping_cart</i>Order List</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="logout.php"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
			
                <?php include "sidebar_user.php"; ?>
				
            </div>
            <
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    <img src="images/copy.jpg" alt="copy" style="width:270px;height:70px;">
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
         
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
            <div class="row clearfix">
			
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="card">
						<div class="header">
							<h2>
								ORDER LIST
							</h2>
						</div>
						<div class="body">
							<div class="row clearfix">
								<div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
									<div class="panel-group" id="accordion_17" role="tablist" aria-multiselectable="true">
									
										<!-- start panel 1-->
										<div class="panel panel-col-grey">
											<div class="panel-heading" role="tab" id="headingOne_17">
												<h4 class="panel-title">
													<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_17" href="#collapseOne_17" aria-expanded="true" aria-controls="collapseOne_17">
															<i class="material-icons">perm_contact_calendar</i>
															ORDER CONFIRMATION
													</a>
												</h4>
											</div>
											<div id="collapseOne_17" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_17">
												<div class="panel-body">
													<div class="table-responsive">
														<table class="table table-hover dashboard-task-infos">
															<thead>
																<tr>
																	<th>Order ID</th>
																	<th>Agent ID</th>
																	<th>Item Name</th>
																	<th>Order Quantity</th>
																	<th>Total</th>
																	<th>Status</th>
																	<th>Action</th>
																</tr>
															</thead>
															
															<?php
															
																$SQLquery5="SELECT * FROM reg_dropship 
																			WHERE id_sponsor='$id'";
																$QueryResult5=mysqli_query($DBconnect,$SQLquery5);																		
																while($row5=mysqli_fetch_array($QueryResult5))
																{																	
																	//$dropship_id[]=$row5["dropship_id"];
																	$dropship_id=$row5["dropship_id"];
																//}

																//$totalDropship = count($dropship_id);
																
																//for ($i=0; $i <$totalDropship ; $i++) {
																	//echo $dropship_id[$i];
																	$SQLquery2="SELECT * FROM dropship_order 
																				WHERE dropship_id='$dropship_id'
																				ORDER BY order_id DESC";
																	$QueryResult2=mysqli_query($DBconnect,$SQLquery2);																																			
																	while($row2=mysqli_fetch_array($QueryResult2))
																	{	
																		$test[] = $row2["order_id"];																	
																		$order_id=$row2["order_id"];
																		$item_id=$row2["item_id"];
																		$order_quantity=$row2["order_quantity"];
																		$order_total=$row2["order_total"];
																		$dropship_id=$row2["dropship_id"];
																		$order_status=$row2["order_status"];
																		
																		
																		$SQLquery3="SELECT * FROM item WHERE item_id='$item_id'";
																		$QueryResult3=mysqli_query($DBconnect,$SQLquery3);																			
																		while($row3=mysqli_fetch_array($QueryResult3))
																		{
																			$item_name=$row3["item_name"];
																		}

																		if($order_status != 'Cancel' && $order_status != 'Confirm') {
															
																?>
																<tbody>
																	<tr>
																		<td><?php echo "$order_id"; ?></td>
																		<td><?php echo "$dropship_id"; ?></td>
																		<td><?php echo "$item_name"; ?></td>
																		<td><?php echo "$order_quantity"; ?></td>
																		<td><?php echo "RM$order_total"; ?></td>
																		<td>
																		<?php
																		
																			if($order_status=="PendingDrop")
																			{
																				?>
																					<span class="label bg-orange">Dropship Payment Pending</span>
																				<?php
																			}
																			else if($order_status=="PayDrop")
																			{
																				?>
																					<span class="label bg-green">Dropship Confirm</span>
																				<?php
																			}
																			else if($order_status=="PayAgent")
																			{
																				?>
																					<span class="label bg-green">Payment Sent HQ</span>
																				<?php
																			}
																			else if($order_status=="Confirm")
																			{
																				?>
																					<span class="label bg-green">HQ Confirm</span>
																				<?php
																			}
																			else if($order_status=="Cancel")
																			{
																				?>
																					<span class="label bg-red">Cancel</span>
																				<?php
																			}
																		
																		?>
																		</td>
																		<td>
																			<form method="get" action="view_order_payment_dropship.php">
																				<input type="hidden" name="order_id" value="<?php echo "$order_id"; ?>">
																				<input type="hidden" name="item_id" value="<?php echo "$item_id"; ?>">
																				<button type="submit" class="btn bg-blue waves-effect btn-xs">View Payment Information</button>
																			</form>
																		</td>
																	</tr>
																</tbody>
																<?php
																		}
																	}
																	//var_dump($test);
																}
																
															?>
														</table>
													</div>
												</div>
											</div>
										</div>
										<!-- end panel 1 -->
										
										<!-- start panel 4 -->									
										<div class="panel panel-col-grey">
											<div class="panel-heading" role="tab" id="headingFour_17">
												<h4 class="panel-title">
													<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_17" href="#collapseFour_17" aria-expanded="false"
													   aria-controls="collapseFour_17">
														<i class="material-icons">archive</i>
														HISTORY ORDER LIST
													</a>
												</h4>
											</div>
											<div id="collapseFour_17" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour_17">
												<div class="panel-body">
													<div class="table-responsive">
														<table class="table table-hover dashboard-task-infos">
															<thead>
																<tr>
																	<th>Order ID</th>
																	<th>Agent ID</th>
																	<th>Item Name</th>
																	<th>Order Quantity</th>
																	<th>Total</th>
																	<th>Status</th>
																	<th>Tracking No.</th>
																	<th>Order Date</th>
																	<th>Action</th>
																</tr>
															</thead>
															
															<?php
															$SQLquery5="SELECT * FROM reg_dropship 
																		WHERE id_sponsor='$id'";
															$QueryResult5=mysqli_query($DBconnect,$SQLquery5);																	
															while($row5=mysqli_fetch_array($QueryResult5))
															{
																$id = $row5['dropship_id'];
																$SQLquery2="SELECT * FROM dropship_order WHERE dropship_id ='$id' ORDER BY order_id DESC";
																$QueryResult2=mysqli_query($DBconnect,$SQLquery2);
																	
																while($row2=mysqli_fetch_array($QueryResult2))
																{
																	$order_id=$row2["order_id"];
																	$item_id=$row2["item_id"];
																	$order_quantity=$row2["order_quantity"];
																	$order_total=$row2["order_total"];
																	$order_status=$row2["order_status"];
																	$order_date=$row2["order_date"];
																	$dropship_id=$row2["dropship_id"];
																	
																	$SQLquery3="SELECT * FROM item WHERE item_id='$item_id'";
																	$QueryResult3=mysqli_query($DBconnect,$SQLquery3);																		
																	while($row3=mysqli_fetch_array($QueryResult3))
																	{
																		$item_name=$row3["item_name"];
																	}
																		
																	$SQLquery4="SELECT * FROM shipping WHERE order_id='$order_id'";
																	$QueryResult4=mysqli_query($DBconnect,$SQLquery4);																		
																	while($row4=mysqli_fetch_array($QueryResult4))
																	{
																		$tracking_no=$row4["tracking_no"];
																	}

																	if ($order_status == 'Cancel' || $order_status == 'Confirm') {
														
															?>
															<tbody>
																<tr>
																	<td><?php echo "$order_id"; ?></td>
																	<td><?php echo "$dropship_id"; ?></td>
																	<td><?php echo "$item_name"; ?></td>
																	<td><?php echo "$order_quantity"; ?></td>
																	<td><?php echo "RM$order_total"; ?></td>
																	<td>
																		<?php
																		
																			if($order_status=="PendingDrop")
																			{
																				?>
																					<span class="label bg-orange">Dropship Payment Pending</span>
																				<?php
																			}
																			else if($order_status=="PayDrop")
																			{
																				?>
																					<span class="label bg-green">Dropship Confirm</span>
																				<?php
																			}
																			else if($order_status=="PayAgent")
																			{
																				?>
																					<span class="label bg-green">Payment Sent HQ</span>
																				<?php
																			}
																			else if($order_status=="Confirm")
																			{
																				?>
																					<span class="label bg-green">HQ Confirm</span>
																				<?php
																			}
																			else if($order_status=="Cancel")
																			{
																				?>
																					<span class="label bg-red">Cancel</span>
																				<?php
																			}
																		
																		?>
																	</td>
																	<td>
																		<?php
																		
																			if($tracking_no==NULL)
																			{
																				?>
																					<span class="label bg-red">NO DATA</span>
																				<?php
																			}
																			else
																			{
																				echo "$tracking_no";
																			}
																		
																		?>
																	</td>
																	<td><?php echo	"$order_date"; ?></td>
																	<td>
																		<form method="get" action="view_dropship_order_information.php">
																			<input type="hidden" name="order_id" value="<?php echo "$order_id"; ?>">
																			<input type="hidden" name="item_id" value="<?php echo "$item_id"; ?>">
																			<button type="submit" class="btn bg-light-green waves-effect btn-xs">View Order Information</button>
																		</form>
																	</td>
																</tr>
															</tbody>
															<?php															
																	}
																}
															}
															?>
														</table>
													</div>
												</div>
											</div>
										</div>
										<!-- end panel 4 -->
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			
            </div>
			
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="plugins/raphael/raphael.min.js"></script>
    <script src="plugins/morrisjs/morris.js"></script>

    <!-- ChartJs -->
    <script src="plugins/chartjs/Chart.bundle.js"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="plugins/flot-charts/jquery.flot.js"></script>
    <script src="plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="plugins/flot-charts/jquery.flot.time.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/index.js"></script>

    <!-- Demo Js -->
    <script src="js/demo.js"></script>
	
	<script>
		function isNumber(evt) {
		evt = (evt) ? evt : window.event;
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
		return true;
	}
	</script>
	
<?php
					
		}
		mysqli_close($DBconnect);
	}
}
?>
</body>

</html>