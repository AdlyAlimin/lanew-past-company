<!DOCTYPE html>
	<?php
session_start();
if($_SESSION['email']=="")
{
	header("location: index.html");
}
else
{
	include("inc_db.php");
	if($DBconnect!==FALSE);
	{
		$SQLquery="SELECT * FROM user WHERE email='$_SESSION[email]'";
		=mysqli_query($DBconnect,$SQLquery);
		
		while(($row=mysqli_fetch_array($QueryResult,MYSQLI_ASSOC))!==FALSE)
		{
			$id=$row["no_id"];
			$email=$_SESSION['email'];
?>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | LANEW Management System</title>
    <!-- Favicon-->
    <link rel="icon" href="lanew_icon.jpeg" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="css/themes/all-themes.css" rel="stylesheet" />
		
</head>


<body class="theme-amber">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">LANEW</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <?php 
					
						$SQLquery6="SELECT * FROM verify_user WHERE sponsor_id='$id'";
						$QueryResult6=mysqli_query( $DBconnect,$SQLquery6);
							
						while(($row6=mysqli_fetch_array($QueryResult6,MYSQLI_ASSOC))!==FALSE)
						{
							if($row6['dp_image']=="")
							{
							?>
								<img src="images/user.png" width="48" height="48" alt="User" />
							<?php
							}
							else
							echo '<img src="data:image/jpeg;base64,'.base64_encode( $row6['dp_image'] ).'" width="48" height="48"/>';
						}
					?>
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo "$id"; ?></div>
                    <div class="email"><?php echo "$email"; ?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="profile.php"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="view_sponsor.php"><i class="material-icons">group</i>Sponsor</a></li>
                            <li><a href="view_order_list.php"><i class="material-icons">shopping_cart</i>Order List</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="logout.php"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
			
                <?php include "sidebar_user.php"; ?>
				
            </div>
            <
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    <img src="images/copy.jpg" alt="copy" style="width:270px;height:70px;">
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
         
    </section>

    <section class="content">
	<?php
	
		$dropship_id=$_GET['dropship_id'];
		$id=$_SESSION['id'];
		
		$SQLquery="SELECT * FROM dropship WHERE dropship_id='$dropship_id'";
		$QueryResult=mysqli_query( $DBconnect,$SQLquery);
		
		while(($row=mysqli_fetch_array($QueryResult,MYSQLI_ASSOC))!==FALSE)
		{
			
	?>
        <div class="container-fluid">
            <div class="block-header">
                <h2>UPDATE PROFILE AGENT</h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                FILL FORM
                            </h2>
                        </div>
                        <div class="body">
                            <form name="update" id="sign_up" method="GET" action="update_profile_dropship.php">
							<input type="hidden" name="dropship_id" value="<?php echo $dropship_id; ?>">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="material-icons">person</i>
									</span>
									<div class="form-line">
										<input type="text" class="form-control" name="nama" placeholder="Name Penuh" value="<?php echo $row["nama"]; ?>" >
									</div>
								</div>
								<div class="input-group">
									<span class="input-group-addon">
										<i class="material-icons">email</i>
									</span>
									<div class="form-line">
										<input type="email" class="form-control" name="email" placeholder="Email" value="<?php echo $row["email"]; ?>" >
									</div>
								</div>
								<div class="input-group">
									<span class="input-group-addon">
										<i class="material-icons">contact_mail</i>
									</span>
									<div class="form-line">
										<input type="text" class="form-control" name="no_ic" placeholder="No. IC" onkeypress="return isNumber(event)" value="<?php echo $row["no_ic"]; ?>" >
									</div>
								</div>
								<div class="input-group">
									<span class="input-group-addon">
										<i class="material-icons">phone</i>
									</span>
									<div class="form-line">
										<input type="text" class="form-control" name="no_tel" placeholder="No. Telefon" value="<?php echo $row["no_tel"]; ?>" >
									</div>
								</div>
								<div class="input-group">
									<span class="input-group-addon">
										<i class="material-icons">home</i>
									</span>
									<div class="form-line">
										<input type="text" class="form-control" name="alamat" placeholder="Alamat" value="<?php echo $row["alamat"]; ?>" >
									</div>
								</div>
								
								<button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">UPDATE</button>

							</form>
							</br>
								<?php
									if($row["user_status"]=="Active")
									{
										?>
										<form method="get" action="suspend_dropship.php">
											<input type="hidden" name="dropship_id" value="<?php echo $dropship_id; ?>">
											<input type="hidden" name="user_status" value="Suspended">
											<button class="btn btn-block btn-lg bg-red waves-effect" type="submit">SUSPEND AGENT</button>
										</form>
										<?php
									}
									else if($row["user_status"]=="Suspended")
									{
										?>
										<form method="get" action="suspend_dropship.php">
											<input type="hidden" name="dropship_id" value="<?php echo $dropship_id; ?>">
											<input type="hidden" name="user_status" value="Active">
											<button class="btn btn-block btn-lg bg-green waves-effect" type="submit">ACTIVE AGENT</button>
										</form>
										<?php
									}
								?>
								
                        </div>
                    </div>
                </div>
            </div>

        </div>
		<?php
		}
		?>
    </section>

    <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="plugins/raphael/raphael.min.js"></script>
    <script src="plugins/morrisjs/morris.js"></script>

    <!-- ChartJs -->
    <script src="plugins/chartjs/Chart.bundle.js"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="plugins/flot-charts/jquery.flot.js"></script>
    <script src="plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="plugins/flot-charts/jquery.flot.time.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/index.js"></script>

    <!-- Demo Js -->
    <script src="js/demo.js"></script>
	
	<script>
		function isNumber(evt) {
		evt = (evt) ? evt : window.event;
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
		return true;
	}
	</script>
	
<?php
					
		}
		mysqli_close($DBconnect);
	}
}
?>
</body>

</html>