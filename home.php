<!DOCTYPE html>
	<?php
session_start();
if($_SESSION['email']=="")
{
	header("location: index.html");
}
else
{
	include("inc_db.php");
	if($DBconnect!==FALSE)
	{
		$SQLquery="SELECT * FROM user WHERE email='$_SESSION[email]'";
		$QueryResult=mysqli_query($DBconnect,$SQLquery);
		
		while(($row=mysqli_fetch_array($QueryResult,MYSQLI_ASSOC)))
		{
			$id=$row["no_id"];
			$_SESSION['id']=$row["no_id"];
			$nama=$row["nama"];
			$no_ic=$row["no_ic"];
			$alamat=$row["alamat"];
			$email=$row["email"];
			$no_tel=$row["no_tel"];
?>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | LANEW Management System</title>
    <!-- Favicon-->
    <link rel="icon" href="lanew_icon.jpeg" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="css/themes/all-themes.css" rel="stylesheet" />
	<style>
	blink {
    -webkit-animation: 2s linear infinite condemed_blink_effect; // for android
    animation: 2s linear infinite condemed_blink_effect;
}
@-webkit-keyframes condemed_blink_effect { // for android
    0% {
        visibility: hidden;
    }
    50% {
        visibility: hidden;
    }
    100% {
        visibility: visible;
    }
}
@keyframes condemed_blink_effect {
    0% {
        visibility: hidden;
    }
    50% {
        visibility: hidden;
    }
    100% {
        visibility: visible;
    }
}
	</style>
</head>

<body class="theme-amber">
    <!-- Page Loader -->
 
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">LANEW</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
				<div class="image">
					<?php 
					
						$SQLquery6="SELECT * FROM verify_user WHERE sponsor_id='$id'";
						$QueryResult6=mysqli_query($DBconnect,$SQLquery6);
							
						$row6=mysqli_fetch_array($QueryResult6,MYSQLI_ASSOC);
						
							if($row6['dp_image']=="")
							{
							?>
								<img src="images/user.png" width="48" height="48" alt="User" />
							<?php
							}
							else
							echo '<img src="data:image/jpeg;base64,'.base64_encode( $row6['dp_image'] ).'" width="48" height="48"/>';
						
					?>
				</div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo "$id"; ?></div>
                    <div class="email"><?php echo "$email"; ?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="profile.php"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="view_sponsor.php"><i class="material-icons">group</i>Agent List</a></li>
                            <li><a href="view_order_list.php"><i class="material-icons">shopping_cart</i>Order List</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="logout.php"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
			
                <?php include "sidebar_user.php"; ?>
				
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    <img src="images/copy.jpg" alt="copy" style="width:270px;height:70px;">
                </div>
            </div>
			
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
            <div class="row clearfix js-sweetalert" >
                <!-- Visitors -->
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="body bg-amber">
                            <div class="m-b--35 font-bold">PROFILE DETAILS</div>
                            <ul class="dashboard-stat-list">
                                <li>
                                    <u>Nama</u>
									</br>
                                    <span><b><?php echo "$nama"; ?></b></span>
                                </li>
                                <li>
                                    <u>No IC</u>
									</br>
                                    <span><b><?php echo "$no_ic"; ?></b></span>
                                </li>
                                <li>
                                    <u>Alamat</u>
									</br>
                                    <span><b><?php echo "$alamat"; ?></b></span>
                                </li>
								<li>
                                    <u>No Telefon</u>
									</br>
                                    <span><b><?php echo "$no_tel"; ?></b></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- #END# Visitors -->
				<?php
				
					$SQLquery1="SELECT * FROM bonus WHERE no_id='$id'";
					$QueryResult1=mysqli_query($DBconnect,$SQLquery1);
					
					while(($row1=mysqli_fetch_array($QueryResult1,MYSQLI_ASSOC)))
					{
						$sponsor=$row1["sponsor"];
						$monthly=$row1["monthly"];
						$yearly=$row1["yearly"];				
				
				?>
	
	
                <!-- Latest Social Trends -->
                <!-- <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="body bg-grey">
                            <div class="m-b--35 font-bold">LATEST BONUS POINTS</div>
                            <ul class="dashboard-stat-list">
                                <li>
                                
                                    <u>Recruitment Bonus</u>
                                    <div class="help-tip"><p><b>Recruitment Bonus:</b><br> Bonus ini akan diperolehi sekiranya anda mendaftarkan sub agen di bawah anda.
                                    Bonus akan di kira berdasarkan bilangan agen yang anda daftarkan.RM 20 akan di perolehi sekiranya pendaftaran di buat</p></div>	
                                    							</br>
                                    <span><b>RM <?php echo "$sponsor"; ?></b></span>
									<span class="pull-right">
										<?php 
											if($sponsor>=1)
											{
												?>
												
												<button type="button" class="btn btn-default waves-effect" data-toggle="modal" data-target="#smallModal">WITHDRAW</button>
												<!-- Small Size -->
												<div class="modal fade" id="smallModal" tabindex="-1" role="dialog">
													<div class="modal-dialog modal-sm" role="document">
														<div class="modal-content">
															<form name="withdraw" method="get" action="withdraw.php">
																<input type="hidden" name="withdraw_type" value="recruitment">
																<input type="hidden" name="withdraw_total" value="<?php echo "$sponsor"; ?>">
																<div class="modal-header">
																	<h4 class="modal-title" id="smallModalLabel">Withdraw Comfirmation</h4>
																</div>
																<div class="modal-body">
																
																	Withdraw Recruitment Bonus?
																
																</div>
																<div class="modal-footer">
																	<button type="submit" class="btn btn-link waves-effect">CONFIRM</button>
																	<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
																</div>
															</form>
														</div>
													</div>
												</div>
												
												<?php
											}
											else
											{
												?>
												<button class="btn btn-default waves-effect" disabled="disabled">WITHDRAW</button>
												<?php
											}
										
										?>
									</span>
                                </li>
                                <li>
                                    <u>Personal Sales Bonus</u>
									</br>
                                    <span><b>RM <?php echo "$monthly"; ?></b></span>
									<span class="pull-right">
										<?php 
											if($monthly>=1)
											{
												?>
												<button type="button" class="btn btn-default waves-effect" data-toggle="modal" data-target="#smallModal1">WITHDRAW</button>
												<!-- Small Size -->
												<div class="modal fade" id="smallModal1" tabindex="-1" role="dialog">
													<div class="modal-dialog modal-sm" role="document">
														<div class="modal-content">
															<form name="withdraw" method="get" action="withdraw.php">
																<input type="hidden" name="withdraw_type" value="monthly">
																<input type="hidden" name="withdraw_total" value="<?php echo "$monthly"; ?>">
																<div class="modal-header">
																	<h4 class="modal-title" id="smallModalLabel">Withdraw Comfirmation</h4>
																</div>
																<div class="modal-body">
																
																	Withdraw Monthly Bonus?
																
																</div>
																<div class="modal-footer">
																	<button type="submit" class="btn btn-link waves-effect">CONFIRM</button>
																	<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
																</div>
															</form>
														</div>
													</div>
												</div>
												<?php
											}
											else
											{
												?>
												<button class="btn btn-default waves-effect" disabled="disabled">WITHDRAW</button>
												<?php
											}
										
										?>
									</span>
                                </li>
                                <li>
                                    <u>Group Sales Bonus</u>
									</br>
                                    <span><b>RM <?php echo "$yearly"; ?></b></span>
									<span class="pull-right">
										<?php 
											if($yearly>=1)
											{
												?>
												<button type="button" class="btn btn-default waves-effect" data-toggle="modal" data-target="#smallModal2">WITHDRAW</button>
												<!-- Small Size -->
												<div class="modal fade" id="smallModal2" tabindex="-1" role="dialog">
													<div class="modal-dialog modal-sm" role="document">
														<div class="modal-content">
															<form name="withdraw" method="get" action="withdraw.php">
																<input type="hidden" name="withdraw_type" value="yearly">
																<input type="hidden" name="withdraw_total" value="<?php echo "$yearly"; ?>">
																<div class="modal-header">
																	<h4 class="modal-title" id="smallModalLabel">Withdraw Comfirmation</h4>
																</div>
																<div class="modal-body">
																
																	Withdraw Yearly Bonus?
																
																</div>
																<div class="modal-footer">
																	<button type="submit" class="btn btn-link waves-effect">CONFIRM</button>
																	<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
																</div>
															</form>
														</div>
													</div>
												</div>
												<?php
											}
											else
											{
												?>
												<button class="btn btn-default waves-effect" disabled="disabled">WITHDRAW</button>
												<?php
											}
										
										?>
									</span>
                                </li>
                                <li>
                                    &nbsp
									</br>
                                    <span><b>&nbsp</b></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div> -->

                <!-- #END# Latest Social Trends -->
                <!-- Answered Tickets -->
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="body bg-amber">
                            <div class="font-bold m-b--35"><img src="images/hello.gif" alt="copy" style="width:45px;height:30px;">PENGUMUMAN !!!!!!!!</div>
                            <ul class="dashboard-stat-list">
                                <li>
                                    <font size=3> Perhatian kepada semua agent yg berdaftar,sila kemaskini data peribadi anda dan masukkan data akaun bank anda untuk urusan withdraw bonus dan dll.sekian <small>admin</small></font>
                                </li>
                                <li>
                                 
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- #END# Answered Tickets -->
            </div>

            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="card">
                        <div class="header">
                            <h2>Agent List</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
                                            <th>No. ID</th>
											<th>Name</th>
                                            <th>Status</th>
											<th>Action</th>
                                        </tr>
                                    </thead>
									
									<?php
			
										$SQLquery2="SELECT * FROM sponsor WHERE id_sponsor='$id' LIMIT 10";
										$QueryResult2=mysqli_query($DBconnect,$SQLquery2);
											
										while(($row2=mysqli_fetch_array($QueryResult2,MYSQLI_ASSOC)))
										{
											$reg_id=$row2["id_reg"];
											
											$SQLquery3="SELECT * FROM user WHERE no_id='$reg_id'";
											$QueryResult3=mysqli_query($DBconnect,$SQLquery3);
												
											while(($row3=mysqli_fetch_array($QueryResult3,MYSQLI_ASSOC)))
											{
												$nama1=$row3["nama"];
												$user_status=$row3["user_status"];
								
									?>
									<tbody>
                                        <tr>
                                            <td><?php echo "$reg_id"; ?></td>
											<td><?php echo "$nama1"; ?></td>
                                            <td>
											<?php 
												if($user_status=="Active")
												{
													?>
													<span class="label bg-green">Active</span>
													<?php
												}
												else if($user_status=="Suspended")
												{
													?>
													<span class="label bg-red">Suspended</span>
													<?php
												}
											?>
											</td>
											<td>
											<form method="get" action="edit_sponsor.php">
												<input type="hidden" name="user_id" value="<?php echo "$reg_id"; ?>">
												<button type="submit" class="btn bg-blue waves-effect btn-xs">Edit Profile</button>
											</form>
											</br>
											</td>
                                        </tr>
                                    </tbody>
									<?php
											}
										}
									
									?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
				
				<!-- Task Info1 -->
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <?php include "top_agent_display.php"; ?>
                </div>
                <!-- #END# Task Info1 -->
				
            </div>
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="plugins/raphael/raphael.min.js"></script>
    <script src="plugins/morrisjs/morris.js"></script>

    <!-- ChartJs -->
    <script src="plugins/chartjs/Chart.bundle.js"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="plugins/flot-charts/jquery.flot.js"></script>
    <script src="plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="plugins/flot-charts/jquery.flot.time.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/index.js"></script>

    <!-- Demo Js -->
    <script src="js/demo.js"></script>
	
<?php
					}
		}
		mysqli_close($DBconnect);
	}
}
?>
</body>

</html>