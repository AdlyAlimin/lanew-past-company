<!DOCTYPE html>
<?php
session_start();
if($_SESSION['email']=="")
{
	header("location: index.html");
}
else
{
	include("inc_db.php");
	if($DBconnect!==FALSE);
	{
		$SQLquery="SELECT * FROM user WHERE email='$_SESSION[email]'";
		$QueryResult=mysqli_query($DBconnect,$SQLquery);
		
		while($row=mysqli_fetch_array($QueryResult))
		{
			$id=$row["no_id"];
			$email=$_SESSION['email'];
			$item_id=$_GET['item_id'];
			$order_id=$_GET['order_id'];
?>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | LANEW Management System</title>
    <!-- Favicon-->
    <link rel="icon" href="lanew_icon.jpeg" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="css/themes/all-themes.css" rel="stylesheet" />
<script>
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>	
<style type="text/css" media="print">
.noprint{
        display:none;
}
                </style>
</head>

<body class="theme-amber">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">LANEW</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <?php 
					
						$SQLquery6="SELECT * FROM verify_user WHERE sponsor_id='$id'";
						$QueryResult6=mysqli_query($DBconnect,$SQLquery6);
							
						while($row6=mysqli_fetch_array($QueryResult6))
						{
							if($row6['dp_image']=="")
							{
							?>
								<img src="images/user.png" width="48" height="48" alt="User" />
							<?php
							}
							else
							echo '<img src="data:image/jpeg;base64,'.base64_encode( $row6['dp_image'] ).'" width="48" height="48"/>';
						}
					?>
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo "$id"; ?></div>
                    <div class="email"><?php echo "$email"; ?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="profile.php"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="view_sponsor.php"><i class="material-icons">group</i>Agent List</a></li>
                            <li><a href="view_order_list.php"><i class="material-icons">shopping_cart</i>Order List</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="logout.php"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
			
                <?php include "sidebar_user.php"; ?>
				
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    <img src="images/copy.jpg" alt="copy" style="width:270px;height:70px;">
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>
    <section class="content" id="printableArea">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Order Item</h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Order Form
                            </h2>
                        </div>
                        <div class="body">
						<?php
							$sql="SELECT * FROM item WHERE item_id='$item_id'";
							$query=mysqli_query($DBconnect,$sql);
							
							while($row2=mysqli_fetch_row($query))
							{
								$sql1="SELECT * FROM order_list WHERE order_id='$order_id'";
								$query1=mysqli_query($DBconnect,$sql1);
								
								while(($row1=mysqli_fetch_row($query1))!==FALSE)
								{
									$order_id=$row1[0];
						?>
							

								<div class="input-group">
									<div>
									<center>
										<?php echo '<img src="data:image/jpeg;base64,'.base64_encode( $row2[2] ).'" width="170" height="170"/>'; ?>
									</center>
									</br>
									</div>
								</div>
								<h4>Product Details</h4>
								<div class="input-group">
									<div>
										<b>Product Name</b> : <?php echo $row2[3]; ?>
									</div>
								</div>
								<div class="input-group">
									<div>
										<b>Product Description</b> : <?php echo $row2[4]; ?>
									</div>
								</div>
								<div class="input-group">
									<div>
										<b>Order Total Price</b> : RM<?php echo $row1[4]; ?>
									</div>
								</div>
								<div class="input-group">
									<div>
										<b>Order Quantity</b> : <?php echo $row1[3]; ?> Units
									</div>
								</div>
								<div>
								<?php
									if($row1[7]==NULL)
									{
										?>
										
									<button type="button" class="btn bg-amber waves-effect noprint" data-toggle="modal" data-target="#smallModal1">VIEW RECEIPT</button>
									<!-- Small Size -->
									<div class="modal fade" id="smallModal1" tabindex="-1" role="dialog">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
															
												<input type="hidden" name="withdraw_type" value="sponsor">
												<div class="modal-header">
													<h4 class="modal-title" id="smallModalLabel">Receipt Comfirmation</h4>
												</div>
												<div class="modal-body">
													<center>
														<p class="col-red">NO DATA IN DATABASE</p>
													</center>
												</div>
											<div class="modal-footer">
																	
											</div>
								
											</div>
										</div>
									</div>

										<?php
									}
									else
									{
										?>
										<button type="button" class="btn bg-amber waves-effect noprint" data-toggle="modal" data-target="#smallModal">VIEW RECEIPT</button>
											<!-- Small Size -->
											<div class="modal fade" id="smallModal" tabindex="-1" role="dialog">
												<div class="modal-dialog" role="document">
													<div class="modal-content">
																	
														<input type="hidden" name="withdraw_type" value="sponsor">
														<div class="modal-header">
															<h4 class="modal-title" id="smallModalLabel">Receipt Comfirmation</h4>
														</div>
														<div class="modal-body">
															<center>
																<?php echo '<img src="data:image/jpeg;base64,'.base64_encode( $row1[7] ).'" width="250" height="350"/>'; ?>
															</center>
														</div>
													<div class="modal-footer">
																			
													</div>
										
													</div>
												</div>
											</div>
									<?php
									
									}
									?>
								</div>
								<div>
									</br>
									<form method="get" action="print_invoice.php">
										<input type="hidden" name="order_id" value="<?php echo "$order_id"; ?>">
										<input type="hidden" name="item_name" value="<?php echo $row2[3]; ?>">
										<input type="hidden" name="item_price" value="<?php echo $row2[6];; ?>">
										<input type="hidden" name="order_quantity" value="<?php echo $row1[3]; ?>">
										
										<button type="submit" class="btn bg-amber waves-effect">PRINT INVOICE</button>
									</form>
									
								</div>


						<?php

								}
							
							
						?>
                        </div>
                    </div>
				</div>
				
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Shipping Information
                            </h2>
                        </div>
                        <div class="body">
						<?php
							$sql8="SELECT * FROM shipping WHERE order_id='$order_id'";
							$query8=mysqli_query($DBconnect,$sql8);
							
							//$GLOBALS['trackingno'] = $row8["tracking_no"];
							
							
							while(($row8=mysqli_fetch_array($query8,MYSQLI_ASSOC))!==FALSE)
							{
								$trackingno = $row8["tracking_no"];
						?>

								<h4>Shipping Details</h4>
								<div class="input-group">
									<div>
										<b>Recipient Name</b> : <?php echo $row8['recipient_name']; ?>
									</div>
								</div>
								<div class="input-group">
									<div>
										<b>Recipient Telephone Number</b> : <?php echo $row8['recipient_no_tel']; ?>
									</div>
								</div>
								<div class="input-group">
									<div>
										<b>Recipient Address Line 1</b> : <?php echo $row8['recipient_address1']; ?>
									</div>
								</div>
								<div class="input-group">
									<div>
										<b>Recipient Address Line 2</b> : <?php echo $row8['recipient_address2']; ?>
									</div>
								</div>
								<div class="input-group">
									<div>
										<b>Recipient Postcode</b> : <?php echo $row8["recipient_postcode"]; ?>
									</div>
								</div>
								<div class="input-group">
									<div>
										<b>Recipient City</b> : <?php echo $row8["recipient_city"]; ?>
									</div>
								</div>
								<div class="input-group">
									<div>
									<?php
									if($row8["tracking_no"]==NULL)
									{
										echo "NO TRACKING NUMBER";
									}
									else
									{
									?>
										<b>Tracking Number</b> : <?php echo $row8["tracking_no"]; ?>
									<?php
									}
									?>
									</div>
								</div>
								<div class="input-group">
									<div>
										</br>
										<p class="col-red">* UPDATE WHEN NEEDED ONLY!!!</p>
										<button type="button" class="btn btn-block btn-lg bg-lime waves-effect" data-toggle="modal" data-target="#defaultModal">UPDATE SHIPPING DETAIL</button>
										<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h4 class="modal-title" id="defaultModalLabel">UPDATE SHIPPING DETAIL</h4>
													</div>
													<div class="modal-body">
													<form name="update" method="get" action="update_shipping_detail1.php">
													<input type="hidden" name="order_id" value="<?php echo $order_id; ?>">
													<input type="hidden" name="item_id" value="<?php echo $item_id; ?>">
														<div class="input-group">
															<span class="input-group-addon">
																<i class="material-icons">person</i>
															</span>
															<div class="form-line">
																<input type="text" class="form-control" name="recipient_name" value="<?php echo $row8['recipient_name']; ?>" style="text-transform: capitalize;" >
															</div>
														</div>
														<div class="input-group">
															<span class="input-group-addon">
																<i class="material-icons">call</i>
															</span>
															<div class="form-line">
																<input type="text" class="form-control" name="recipient_tel_no" value="<?php echo $row8['recipient_no_tel']; ?>" style="text-transform: capitalize;" >
															</div>
														</div>
														<div class="input-group">
															<span class="input-group-addon">
																<i class="material-icons">place</i>
															</span>
															<div class="form-line">
																<input type="text" class="form-control" name="recipient_address1" value="<?php echo $row8['recipient_address1']; ?>" style="text-transform: capitalize;" >
															</div>
														</div>
														<div class="input-group">
															<span class="input-group-addon">
																<i class="material-icons">place</i>
															</span>
															<div class="form-line">
																<input type="text" class="form-control" name="recipient_address2" value="<?php echo $row8['recipient_address2']; ?>" style="text-transform: capitalize;" >
															</div>
														</div>
														<div class="input-group">
															<span class="input-group-addon">
																<i class="material-icons">place</i>
															</span>
															<div class="form-line">
																<input type="text" class="form-control" name="recipient_postcode" value="<?php echo $row8["recipient_postcode"]; ?>" style="text-transform: capitalize;" >
															</div>
														</div>
														<div class="input-group">
															<span class="input-group-addon">
																<i class="material-icons">place</i>
															</span>
															<div class="form-line">
																<input type="text" class="form-control" name="recipient_city" value="<?php echo $row8["recipient_city"]; ?>" style="text-transform: capitalize;" >
															</div>
														</div>
														
													</div>
													<div class="modal-footer">
														<button type="submit" class="btn bg-green waves-effect">UPDATE SHIPPING DETAIL</button>
														<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
													</div>
													</form>
												</div>
											</div>
										</div>

							<?php
							}	
							}
							?>
									
									</div>
								</div>	

                        </div>
                    </div>
                </div>
				
            </div>
			
			<div class="row clearfix">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Tracking Item</h2>
                        </div>
                        <div class="body">
							<div id="embedTrack"></div>
							<script src="//www.tracking.my/track-button.js"></script>
							<script>
							TrackButton.embed({
								selector: "#embedTrack",
								tracking_no: "<?php echo $trackingno; ?>"
							});
							</script>
                        </div>
                    </div>
                </div>
            </div>
			
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="plugins/raphael/raphael.min.js"></script>
    <script src="plugins/morrisjs/morris.js"></script>

    <!-- ChartJs -->
    <script src="plugins/chartjs/Chart.bundle.js"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="plugins/flot-charts/jquery.flot.js"></script>
    <script src="plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="plugins/flot-charts/jquery.flot.time.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
	<script src="js/script.js"></script>
    <script src="js/pages/index.js"></script>
	

    <!-- Demo Js -->
    <script src="js/demo.js"></script>
<?php
	}		
		}
		mysqli_close($DBconnect);
	
}
?>
</body>

</html>