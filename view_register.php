<!DOCTYPE html>

<?php
session_start();

include("inc_db.php");
if($DBconnect!==FALSE);
{
	$query="SELECT * FROM user WHERE no_id='".$_SESSION['new1']."'";
	$result=mysqli_query($DBconnect,$query) or die ("Verification error");;
			
	if(mysqli_num_rows($result)>0)
	{
		$row=mysqli_fetch_array($result,MYSQLI_ASSOC)
?>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign Up | LANEW Management System</title>
    <!-- Favicon-->
    <link rel="icon" href="lanew_icon.jpeg" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body class="signup-page" background="images/bg2.png">
    <div class="signup-box">
        <div class="logo">
            <a href="javascript:void(0);"><b><img src="images/logo2.png" alt="Mountain View" style="width:350px;height:130px;"></b></a>
        </div>
        <div class="card">
            <div class="body">
                    <div class="msg">Register Successful</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div>
							<?php echo "$row[nama]"; ?>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div>
                            <?php echo "$row[email]"; ?>
                        </div>
                    </div>
					<div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">contact_mail</i>
                        </span>
                        <div>
                            <?php echo "$row[no_ic]"; ?>
                        </div>
                    </div>
					<div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">phone</i>
                        </span>
                        <div>
                            <?php echo "$row[no_tel]"; ?>
                        </div>
                    </div>
					<div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">home</i>
                        </span>
                        <div>
                            <?php echo "$row[alamat]"; ?>
                        </div>
                    </div>
					<button onClick="window.location.href='logout.php'" class="btn btn-block btn-lg bg-teal waves-effect" type="submit">SIGN IN</button>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/examples/sign-up.js"></script>
	<?php
	
	}
	mysqli_close($DBconnect);
}
?>
</body>
</html>