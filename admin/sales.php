<!DOCTYPE html>
	<?php
session_start();
if($_SESSION['email']=="")
{
	header("location: index.html");
}
else
{
	include("inc_db.php");
	if($DBconnect!==FALSE);
	{
		$SQLquery="SELECT * FROM user WHERE email='$_SESSION[email]'";
		$QueryResult=mysqli_query($DBconnect, $SQLquery);
		
		while($row=mysqli_fetch_array($QueryResult))
		{
			$id=$row["no_id"];
			$_SESSION['id']=$row["no_id"];
			$email=$row["email"];
?>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | LANEW Management System</title>
    <!-- Favicon-->
    <link rel="icon" href="../lanew_icon.jpeg" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="../plugins/node-waves/waves.css" rel="stylesheet" />
	
	<!-- Bootstrap Material Datetime Picker Css -->
    <link href="../plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />
	
	<!-- Bootstrap Select Css -->
    <link href="../plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
	
	<!-- Wait Me Css -->
    <link href="../plugins/waitme/waitMe.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="../plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="../css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../css/themes/all-themes.css" rel="stylesheet" />
	

	
</head>

<body class="theme-amber">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">LANEW ADMIN SITE</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <?php 
					
						$SQLquery61="SELECT * FROM verify_user WHERE sponsor_id='$id'";
						$QueryResult61=mysqli_query($DBconnect, $SQLquery61);
							
						while($row61=mysqli_fetch_array($QueryResult61))
						{
							if($row61['dp_image']=="")
							{
							?>
								<img src="../images/user.png" width="48" height="48" alt="User" />
							<?php
							}
							else
							echo '<img src="data:image/jpeg;base64,'.base64_encode( $row61['dp_image'] ).'" width="48" height="48"/>';
						}
					?>
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo "$id"; ?></div>
                    <div class="email"><?php echo "$email"; ?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="profile.php"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="view_order_list.php"><i class="material-icons">shopping_cart</i>Order List</a></li>
							<li><a href="view_withdraw_list.php"><i class="material-icons">account_balance_wallet</i>Withdraw List</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="logout.php"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
				
                    <?php include 'side_menu.php'; ?>
					
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    <img src="images/copy.jpg" alt="copy" style="width:270px;height:70px;">
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
			
            <div class="row clearfix">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card"> 
                        <div class="body">
						 <h4>Sales Today</h4>

                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
                                            <th>No</th>
											<th>Order Quantity</th>
											<th>Order Total</th>
                                        </tr>
                                    </thead>
									
									<?php
										
										$today=date("Y-m-d");
										
										$SQLquery2="SELECT * FROM order_list 
													WHERE sponsor_id='$id'
													AND order_date='$today'";
										$QueryResult2=mysqli_query($DBconnect, $SQLquery2);
											
										$tempNum=1;	
										
										while($row2=mysqli_fetch_array($QueryResult2))
										{
											$order_id=$row2['order_id'];
											$order_quantity=$row2['order_quantity'];
											$order_total=$row2['order_total'];

								
									?>
									<tbody>
                                        <tr>
											<td><?php echo "$tempNum"; ?></td>
                                            <td><?php echo "$order_quantity"; ?> Units</td>
											<td>RM<?php echo "$order_total"; ?></td>
                                        </tr>
                                    </tbody>
									<?php
											$tempNum++;
										}
									
									?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			
			<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<form name="report" method="get" action="view_report.php">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Date Picker
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-4">
                                    <div class="form-group">
									Start Date
                                        <div class="form-line">
                                            <input name="start_date" type="date" class="datepicker form-control" placeholder="Please choose a date...">
                                        </div>
                                    </div>
                                </div>
								
								<div class="col-sm-4">
                                    <div class="form-group">
									End Date
                                        <div class="form-line">
                                            <input name="end_date" type="date" class="datepicker form-control" placeholder="Please choose a date...">
                                        </div>
                                    </div>
                                </div>
								
								<div class="col-sm-4">
                                    <div class="form-group">
									Select Product Name
                                        <div>
                                            <select name="product" class="form-control show-tick">
												<option value="">ALL Product</option>
											<?php
											$SQLquery5="SELECT
															item_name, item_id
														FROM
															item";
											$QueryResult5=mysqli_query($DBconnect, $SQLquery5);
																				
											while($row5=mysqli_fetch_array($QueryResult5))
											{
												$item_name=$row5['item_name'];
												$item_id=$row5['item_id'];
											?>
													<option value="<?php echo "$item_id"; ?>"><?php echo "$item_name"; ?></option>
												
											<?php
											
											}
											
											?>
											</select>
                                        </div>
                                    </div>
                                </div>
                            </div>
							
							<div>
                                <button type="submit" class="btn bg-lime waves-effect">
									<i class="material-icons">assignment</i>
									<span>View</span>
								</button>
                            </div>
							
                        </div>
                    </div>
				</form>
                </div>
            </div>
			
			<div class="card">
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<h4>Monthly Sales for ALL Product</h4>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                <li role="presentation" class="active"><a href="#current" data-toggle="tab">Current Month <?php 
											
													$bulan=date("m");
											
													if($bulan==01)
														echo "January";
													else if($bulan==02)
														echo "February";
													else if($bulan==03)
														echo "March";
													else if($bulan==04)
														echo "April";
													else if($bulan==05)
														echo "May";
													else if($bulan==06)
														echo "June";
													else if($bulan==07)
														echo "July";
													else if($bulan==08)
														echo "August";
													else if($bulan==09)
														echo "September";
													else if($bulan==10)
														echo "October";
													else if($bulan==11)
														echo "November";
													else if($bulan==12)
														echo "December";
						
												?> </a></li>
                                <li role="presentation"><a href="#stock" data-toggle="tab">Stock Report</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane animated fadeInUp active" id="current">
                                    
                                    <p>
                                        <?php include 'sales/current.php'; ?>
										
                                    </p>
                                </div>
								
								<div role="tabpanel" class="tab-pane animated fadeInUp" id="stock">
                                    
                                    <p>
                                        <?php include 'sales/stock_report.php'; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
						
                    </div>
                </div>
            </div>
			
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="../plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="../plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="../plugins/jquery-countto/jquery.countTo.js"></script>
	
	<!-- Autosize Plugin Js -->
    <script src="../plugins/autosize/autosize.js"></script>
	
	<!-- Moment Plugin Js -->
    <script src="../plugins/momentjs/moment.js"></script>
	
	<!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="../plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

    <!-- Custom Js -->
    <script src="../js/admin.js"></script>
    <script src="../js/pages/index.js"></script>
	<script src="../js/pages/forms/basic-form-elements.js"></script>
	<script src="../js/print.js"></script>

    <!-- Demo Js -->
    <script src="../js/demo.js"></script>
<?php
					
		}
		mysqli_close($DBconnect);
	}
}
?>
</body>

</html>