<li class="header">MAIN NAVIGATION</li>

<li class="active">
    <a href="home.php">
		<i class="material-icons">home</i>
		<span>Home</span>
	</a>
</li>

<li>
    <a href="view_profile.php">
		<i class="material-icons">person</i>
		<span>Profile</span>
	</a>
</li>

&nbsp&nbsp AGENT 

<li>
	<a href="javascript:void(0);" class="menu-toggle">
		<i class="material-icons">person</i>
			<span>MANAGE AGENT</span>
	</a>
	<ul class="ml-menu">
	     <li>
			<a href="register_downline.php">
				<i class="material-icons">person_add</i>
				<span>Register Agent</span>
			</a>
		</li>
		<li>
			<a href="view_all_agent.php">
				<i class="material-icons">person</i>
				<span>View All Agent</span>
			</a>
		</li>
		<li>
			<a href="view_leader_with_agent.php">
				<i class="material-icons">person</i>
				<span>View Leader With Agent</span>
			</a>
		</li>
		<li>
			<a href="view_agent_under_admin.php">
				<i class="material-icons">person</i>
				<span>View Agent Under Admin</span>
			</a>
		</li>
	</ul>
</li>
<li>
	<a href="javascript:void(0);" class="menu-toggle">
		<i class="material-icons">people</i>
			<span>MANAGE DROPSHIP</span>
	</a>
	<ul class="ml-menu">
	     <li>
			<a href="register_dropship.php">
				<i class="material-icons">group_add</i>
				<span>Register Dropship</span>
			</a>
		</li>
		<li>
			<a href="view_all_dropship.php">
				<i class="material-icons">group</i>
				<span>View All Dropship</span>
			</a>
		</li>
	</ul>
</li>

<li>
	<a href="javascript:void(0);" class="menu-toggle">
		<i class="material-icons">shopping_basket</i>
			<span>MANAGE ITEM</span>
	</a>
	<ul class="ml-menu">
		<li>
			<a href="add_item.php">
				<i class="material-icons">add</i>
				<span>Add New Item</span>
			</a>
		</li>
		<li>
			<a href="display_item.php">
				<i class="material-icons">local_mall</i>
				<span>View Item</span>
			</a>
		</li>
	</ul>
</li>
<li>
	<a href="javascript:void(0);" class="menu-toggle">
		<i class="material-icons">security</i>
			<span>VERIFY AGENT</span>
	</a>
	<ul class="ml-menu">
	     <li>
			<a href="all_verify_agent.php">
				<i class="material-icons">person</i>
				<span>All Verify Agent</span>
			</a>
		</li>
		<li>
			<a href="verify_agent_request.php">
				<i class="material-icons">person</i>
				<span>Verify Agent Request</span>
			</a>
		</li>
	</ul>
</li>
<li>
	<a href="javascript:void(0);" class="menu-toggle">
		<i class="material-icons">shopping_cart</i>
			<span>MANAGE ORDER</span>
	</a>
	<ul class="ml-menu">
		<li>
			<a href="view_order_list.php">
				<i class="material-icons">shopping_cart</i>
				<span>Agent Order List</span>
			</a>
		</li>
		<li>
			<a href="view_order_dropship.php">
				<i class="material-icons">shopping_cart</i>
				<span>Dropship Order List</span>
			</a>
		</li>
	</ul>
</li>

<li>
	<a href="view_item.php">
		<i class="material-icons">add_shopping_cart</i>
		<span>Order Item</span>
	</a>
</li>

<!-- set postage price -->
<li>
	<a href="postage_set.php">
		<i class="material-icons">local_shipping</i>
		<span>Set Postage Cost</span>
	</a>
</li>
<!-- set postage price -->

<li>
	<a href="sales.php">
		<i class="material-icons">trending_up</i>
		<span>Sales</span>
	</a>
</li>

<li>
	<a href="view_withdraw_list.php">
		<i class="material-icons">account_balance_wallet</i>
		<span>Withdraw List</span>
	</a>
</li>
<li>
	<a href="view_withdraw_list.php">
		<i class="material-icons">attach_money</i>
		<span>Maybank2U</span>
	</a>
</li>

