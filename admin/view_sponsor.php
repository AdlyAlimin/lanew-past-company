<!DOCTYPE html>
	<?php
session_start();
if($_SESSION['email']=="")
{
	header("location: index.html");
}
else
{
	include("inc_db.php");
	if($DBconnect!==FALSE);
	{
		$SQLquery="SELECT * FROM user WHERE email='$_SESSION[email]'";
		$QueryResult=mysqli_query($DBconnect, $SQLquery);
		
		while($row=mysqli_fetch_array($QueryResult))
		{
			$id=$row["no_id"];
			$_SESSION['id']=$row["no_id"];
?>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | LANEW Management System</title>
    <!-- Favicon-->
    <link rel="icon" href="../lanew_icon.jpeg" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="../plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="../plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="../css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-red">
    
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">LANEW</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <?php 
					
						$SQLquery61="SELECT * FROM verify_user WHERE sponsor_id='$id'";
						$QueryResult61=mysqli_query($DBconnect, $SQLquery61);
							
						while($row61=mysqli_fetch_array($QueryResult61))
						{
							if($row61['dp_image']=="")
							{
							?>
								<img src="../images/user.png" width="48" height="48" alt="User" />
							<?php
							}
							else
							echo '<img src="data:image/jpeg;base64,'.base64_encode( $row61['dp_image'] ).'" width="48" height="48"/>';
						}
					?>
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo "$id"; ?></div>
                    <div class="email"><?php echo "$_SESSION[email]"; ?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="profile.php"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="view_sponsor.php"><i class="material-icons">group</i>Sponsor</a></li>
                            <li><a href="view_order_list.php"><i class="material-icons">shopping_cart</i>Order List</a></li>
							<li><a href="view_withdraw_list.php"><i class="material-icons">account_balance_wallet</i>Withdraw List</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="logout.php"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
				
                    <?php include 'side_menu.php'; ?>
					
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2016 - 2017 <a href="javascript:void(0);">AdminBSB - Material Design</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.5
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
			
			<div class="row clearfix">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Registed Sponsor List</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
											<th>Sponsor Name</th>
											<th>Email</th>
                                            <th>Status</th>
											<th>Action</th>
                                        </tr>
                                    </thead>
									
									<?php
			
										$SQLquery2="SELECT * FROM sponsor WHERE id_sponsor='$id'";
										$QueryResult2=mysqli_query($DBconnect, $SQLquery2);
											
										while($row2=mysqli_fetch_array($QueryResult2))
										{
											$reg_id=$row2["id_reg"];
											
											$SQLquery3="SELECT * FROM user WHERE no_id='$reg_id'";
											$QueryResult3=mysqli_query($DBconnect, $SQLquery3);
												
											while($row3=mysqli_fetch_array($QueryResult3))
											{
												$nama1=$row3["nama"];
												$user_status=$row3["user_status"];
												$email=$row3["email"];
								
									?>
									<tbody>
                                        <tr>
											<td><?php echo "$nama1"; ?></td>
											<td><?php echo "$email"; ?></td>
                                            <td>
											<?php 
												if($user_status=="Active")
												{
													?>
													<span class="label bg-green">Active</span>
													<?php
												}
												else if($user_status=="Suspended")
												{
													?>
													<span class="label bg-red">Suspended</span>
													<?php
												}
											?>
											</td>
											<td>
												<form method="get" action="view_sponsor_profile.php">
													<input type="hidden" name="user_id" value="<?php echo "$reg_id"; ?>">
													<button type="submit" class="btn bg-blue waves-effect btn-xs">View Sponsor Profile</button>
												</form>
												&nbsp;
												<form method="get" action="edit_sponsor.php">
													<input type="hidden" name="user_id" value="<?php echo "$reg_id"; ?>">
													<button type="submit" class="btn bg-green waves-effect btn-xs">Edit Sponsor Profile</button>
												</form>
											</td>
                                        </tr>
                                    </tbody>
									<?php
											}
										}
									
									?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			
            <div class="row clearfix">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>All Sponsor List</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
											<th>Sponsor Name</th>
											<th>Email</th>
                                            <th>Status</th>
											<th>Action</th>
                                        </tr>
                                    </thead>
									
									<?php
											$SQLquery4="SELECT * FROM user";
											$QueryResult4=mysqli_query($DBconnect, $SQLquery4);
												
											while($row4=mysqli_fetch_array($QueryResult4))
											{
												$sponsor_id=$row4["no_id"];
												$nama1=$row4["nama"];
												$user_status=$row4["user_status"];
												$email=$row4["email"];
								
									?>
									<tbody>
                                        <tr>
											<td><?php echo "$nama1"; ?></td>
											<td><?php echo "$email"; ?></td>
                                            <td>
											<?php 
												if($user_status=="Active")
												{
													?>
													<span class="label bg-green">Active</span>
													<?php
												}
												else if($user_status=="Suspended")
												{
													?>
													<span class="label bg-red">Suspended</span>
													<?php
												}
											?>
											</td>
											<td>
												<form method="get" action="view_sponsor_profile.php">
													<input type="hidden" name="user_id" value="<?php echo "$sponsor_id"; ?>">
													<button type="submit" class="btn bg-blue waves-effect btn-xs">View Sponsor Profile</button>
												</form>
												&nbsp;
												<form method="get" action="edit_sponsor.php">
													<input type="hidden" name="user_id" value="<?php echo "$sponsor_id"; ?>">
													<button type="submit" class="btn bg-green waves-effect btn-xs">Edit Sponsor Profile</button>
												</form>
											</td>
                                        </tr>
                                    </tbody>
									<?php
											}
										
									
									?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="../plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="../plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="../plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="../plugins/raphael/raphael.min.js"></script>
    <script src="../plugins/morrisjs/morris.js"></script>

    <!-- ChartJs -->
    <script src="../plugins/chartjs/Chart.bundle.js"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="../plugins/flot-charts/jquery.flot.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.time.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="../plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Custom Js -->
    <script src="../js/admin.js"></script>
    <script src="../js/pages/index.js"></script>

    <!-- Demo Js -->
    <script src="../js/demo.js"></script>
<?php
					
		}
		mysqli_close($DBconnect);
	}
}
?>
</body>

</html>