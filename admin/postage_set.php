<!DOCTYPE html>
	<?php
session_start();
if($_SESSION['email']=="")
{
	header("location: index.html");
}
else
{
	include("inc_db.php");
	if($DBconnect!==FALSE);
	{
		$SQLquery="SELECT * FROM user WHERE email='$_SESSION[email]'";
        $QueryResult=mysqli_query($DBconnect, $SQLquery);
        //iqbalH - postage
        $conn = $DBconnect;
        //iqbalH - postage
		
		while($row=mysqli_fetch_array($QueryResult,MYSQLI_ASSOC))
		{
			$id=$row["no_id"];
			$_SESSION['id']=$row["no_id"];
			$email=$row["email"];
?>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | LANEW Management System</title>
    <!-- Favicon-->
    <link rel="icon" href="../lanew_icon.jpeg" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="../plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="../plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="../css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-amber">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">LANEW ADMIN SITE</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <?php 
					
						$SQLquery61="SELECT * FROM verify_user WHERE sponsor_id='$id'";
						$QueryResult61=mysqli_query($DBconnect, $SQLquery61);
							
						while($row61=mysqli_fetch_array($QueryResult61,MYSQLI_ASSOC))
						{
							if($row61['dp_image']=="")
							{
							?>
								<img src="../images/user.png" width="48" height="48" alt="User" />
							<?php
							}
							else
							echo '<img src="data:image/jpeg;base64,'.base64_encode( $row61['dp_image'] ).'" width="48" height="48"/>';
						}
					?>
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo "$id"; ?></div>
                    <div class="email"><?php echo "$email"; ?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="profile.php"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="view_order_list.php"><i class="material-icons">shopping_cart</i>Order List</a></li>
							<li><a href="view_withdraw_list.php"><i class="material-icons">account_balance_wallet</i>Withdraw List</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="logout.php"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
				
                    <?php include 'side_menu.php'; ?>
					
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    <img src="images/copy.jpg" alt="copy" style="width:270px;height:70px;">
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>
    <!-- iqbalH - postage -->
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>POSTAGE CHARGE RATE</h2>
            </div>
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="card">
                        <div class="header">
                            <h2> Current Postage Charge/Rate </h2>
                        </div>
						<div class="body table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Location</th>
                                        <th>First Item Rate</th>
                                        <th>Next Item Rate</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $getPostRate = mysqli_query($DBconnect, "SELECT post_sm,post_smplus,post_ss,post_ssplus FROM postage WHERE id='1'");
                                        if(mysqli_num_rows($getPostRate) != 0){
                                            while ($postValue = mysqli_fetch_assoc($getPostRate)) {
                                                //check database value, empty or not
                                                if(empty($postValue['post_sm']))                                                    
                                                    $sm2pass = '0.00';
                                                else
                                                    $sm2pass = $postValue['post_sm'];

                                                //check database value, empty or not
                                                if(empty($postValue['post_smplus']))                                                    
                                                    $smplus2pass = '0.00';
                                                else
                                                    $smplus2pass = $postValue['post_smplus'];

                                                //check database value, empty or not
                                                if(empty($postValue['post_ss']))                                                    
                                                    $ss2pass = '0.00';
                                                else
                                                    $ss2pass = $postValue['post_ss'];
                                                
                                                //check database value, empty or not
                                                if(empty($postValue['post_ssplus']))                                                    
                                                    $ssplus2pass = '0.00';
                                                else
                                                    $ssplus2pass = $postValue['post_ssplus'];                                                
                                            }
                                        }else{
                                            $sm2pass = '0.00';
                                            $smplus2pass = '0.00';                                                
                                            $ss2pass = '0.00';
                                            $ssplus2pass = '0.00';
                                        }
                                    ?>
                                    <tr>
                                        <td>Semenanjung Malaysia</td>
                                        <td>RM <?php echo $sm2pass; ?></td>
                                        <td>RM <?php echo $smplus2pass; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Sabah & Sarawak</td>
                                        <td>RM <?php echo $ss2pass; ?></td>
                                        <td>RM <?php echo $ssplus2pass; ?></td>
                                    </tr>
                                </tbody>
                            </table>
						</div>
					</div>
				</div>
            </div>
            <div class="block-header align-center">
                <h2>OR</h2>
            </div>
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="card">
                        <div class="header">
                            <h2> Set New Rate </h2>
                        </div>
						<div class="body clearfix">
                            <form  action="postage_set_process.php"  method="post"name="set_rate_postage">                                
                                <div class="col-sm-6 col-xs-12">
                                    <label>Postage Rate Semenanjung Malaysia: </label>
                                    </br>
                                    <label for="sm1rate">First Item Rate - RM</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="sm1rate" name="sm1rate" class="form-control" 
                                            <?php
                                            if(isset($sm2pass) && $sm2pass !== '0.00'){
                                                echo 'value="'.$sm2pass.'"';
                                            }else{
                                                echo 'placeholder="7.00"';
                                            }
                                            ?>
                                            >
                                        </div>
                                    </div>
                                    <label for="smPlusrate">Next Item Rate - RM</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="smPlusrate" name="smPlusrate" class="form-control"
                                            <?php
                                            if(isset($smplus2pass) && $smplus2pass !== '0.00'){
                                                echo 'value="'.$smplus2pass.'"';
                                            }else{
                                                echo 'placeholder="5.00"';
                                            }
                                            ?>
                                            >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">                                
                                    <label>Postage Rate Sabah & Sarawak: </label>
                                    </br>                                
                                    <label for="ss1rate">First Item Rate - RM</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="ss1rate" name="ss1rate" class="form-control"
                                            <?php
                                            if(isset($ss2pass) && $ss2pass !== '0.00'){
                                                echo 'value="'.$ss2pass.'"';
                                            }else{
                                                echo 'placeholder="15.00"';
                                            }
                                            ?>
                                            >
                                        </div>
                                    </div>
                                    <label for="ssPlusrate">Next Item Rate - RM</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="ssPlusrate" name="ssPlusrate" class="form-control"
                                            <?php
                                            if(isset($ssplus2pass) && $ssplus2pass !== '0.00'){
                                                echo 'value="'.$ssplus2pass.'"';
                                            }else{
                                                echo 'placeholder="7.50"';
                                            }
                                            ?>
                                            >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-xs-12">
                                    <input type="submit" class="btn bg-amber btn-block btn-md m-t-15 waves-effect" name="set_rate_postage" value="Save NEW Rate">
                                </div>
                            </form>
						</div>
					</div>
				</div>
            </div>
        </div>
    </section>
    <!-- iqbalH - postage -->

    <!-- Jquery Core Js -->
    <script src="../plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="../plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="../plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="../plugins/raphael/raphael.min.js"></script>
    <script src="../plugins/morrisjs/morris.js"></script>

    <!-- ChartJs -->
    <script src="../plugins/chartjs/Chart.bundle.js"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="../plugins/flot-charts/jquery.flot.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.time.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="../plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Custom Js -->
    <script src="../js/admin.js"></script>
    <script src="../js/pages/index.js"></script>

    <!-- Demo Js -->
    <script src="../js/demo.js"></script>
<?php
					
		}
		mysqli_close($DBconnect);
	}
}
?>
</body>

</html>