<!DOCTYPE html>
	<?php
session_start();
if($_SESSION['email']=="")
{
	header("location: index.html");
}
else
{
	include("inc_db.php");
	if($DBconnect!==FALSE);
	{
		$SQLquery="SELECT * FROM user WHERE email='$_SESSION[email]'";
		$QueryResult=mysqli_query($DBconnect, $SQLquery);
		
		while($row=mysqli_fetch_array($QueryResult))
		{
			$id=$row["no_id"];
			$email=$_SESSION['email'];

?>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | Admin LANEW Management System</title>
    <!-- Favicon-->
    <link rel="icon" href="../lanew_icon.jpeg" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="../plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="../plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="../css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-amber">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">LANEW ADMIN SITE</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <?php 
					
						$SQLquery6="SELECT * FROM verify_user WHERE sponsor_id='$id'";
						$QueryResult6=mysqli_query($DBconnect, $SQLquery6);
							
						while($row6=mysqli_fetch_array($QueryResult6))
						{
							if($row6['dp_image']=="")
							{
							?>
								<img src="../images/user.png" width="48" height="48" alt="User" />
							<?php
							}
							else
							echo '<img src="data:image/jpeg;base64,'.base64_encode( $row6['dp_image'] ).'" width="48" height="48"/>';
						}
					?>
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo "$id"; ?></div>
                    <div class="email"><?php echo "$email"; ?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="profile.php"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="view_order_list.php"><i class="material-icons">shopping_cart</i>Order List</a></li>
							<li><a href="view_withdraw_list.php"><i class="material-icons">account_balance_wallet</i>Withdraw List</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="logout.php"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
				
                    <?php include 'side_menu.php'; ?>
					
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    <img src="images/copy.jpg" alt="copy" style="width:270px;height:70px;">
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Register New Item</h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card clearfix">
                        <div class="header">
                            <h2>
                                FILL FORM
                            </h2>
                        </div>
                        <div class="body">
                            <form name="register" id="sign_up" method="post" action="save_item.php" enctype="multipart/form-data">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="material-icons">photo</i>
									</span>
									<div class="form-line">
										<input type="file" class="form-control" name="image">
									</div>
								</div>
								<div class="input-group">
									<span class="input-group-addon">
										<i class="material-icons">local_offer</i>
									</span>
									<div class="form-line">
										<input type="text" class="form-control" name="item_name" placeholder="Item Name" required>
									</div>
								</div>
								<div class="input-group">
									<span class="input-group-addon">
										<i class="material-icons">create</i>
									</span>
									<div class="form-line">
										<input type="text" class="form-control" name="item_dec" placeholder="Item Description" required>
									</div>
								</div>
								<div class="input-group">
									<span class="input-group-addon">
										<i class="material-icons">add</i>
									</span>
									<div class="form-line">
										<input type="text" class="form-control" name="item_quantity" placeholder="Item Quantity" required>
									</div>
								</div>
                                
                                <!-- iqbalH - cost of good calculation -->
                                <div class="input-group">
                                    <span class="input-group-addon">
										<i class="material-icons">inbox</i>
									</span>
									<div>
										<b>Product Type</b> : 
										
										<input name="bulkItem" type="radio" id="radio_1" class="with-gap radio-col-amber" value="yay" />
										<label for="radio_1">Bulk Item</label>
										<input name="bulkItem" type="radio" id="radio_2" class="with-gap radio-col-amber" value="nay" />
										<label for="radio_2">Standalone Item</label>
										
									</div>
									
								
									   
									   
                                    <p class="msg align-left">Select product type for shipping cost calculation</p>
								</div>
								
								<!-- Farid add SUK + variationn -->
								 <div class="input-group">
								      <span class="input-group-addon">
										<i class="material-icons">category</i>
										</span>
											<b>Product Variation</b>
											 <p class="msg align-left">Select product color <input type="color" name="favcolor" value="#ff0000"></p>
									  
											<br>
											  <table  id="table">
										<thead><tr>
										<div class="col-sm-2">
									        <input type="button" value="Add Size" onClick="addRow('dataTable')" /> 
										</div>
										</tr></thead>
									<tbody id="dataTable">
									<tr>
									<td>
									     <div class="col-sm-5 ">
											<input type="text" name="tel[]"  placeholder="Size" class="form-control">
										</div>
										<div class="col-sm-5 ">
											<input type="number" name="tel[]" min="0" placeholder="Quantity" class="form-control">
										</div>
                                   </td>
								   </tr>
									 </tbody>
									
									   </table>
									   	</div>
									   	
									   	<div class="input-group">
									<span class="input-group-addon">
										<i class="material-icons">chrome_reader_mode</i>
									</span>
									<div class="form-line">
										<input type="text" class="form-control" name="suk_no" placeholder="SUK No" required>
									</div>
								</div>
								
								
									   	
                                <div class="input-group">
									<span class="input-group-addon">
										<i class="material-icons">attach_money</i>
									</span>
									<div class="form-line">
										<input type="text" class="form-control" name="item_cost_of_good" placeholder="Cost of Good" required>
									</div>
								</div>
                                <!-- iqbalH - cost of good calculation -->

								<hr>
								<h4>Price Section</h4>
								
								<!-- agent price -->
								<div class="input-group">
									<span class="input-group-addon">
										<i class="material-icons">attach_money</i>
									</span>
									<div class="form-line">
										<input type="text" class="form-control" name="item_price_agent_sm" placeholder=" Agent Price (SM)" required>
									</div>
								</div>
								<div class="input-group">
									<span class="input-group-addon">
										<i class="material-icons">attach_money</i>
									</span>
									<div class="form-line">
										<input type="text" class="form-control" name="item_price_agent_ss" placeholder=" Agent Price (SS)" required>
									</div>
								</div>
								
								<!-- admin price -->
								<div class="input-group">
									<span class="input-group-addon">
										<i class="material-icons">attach_money</i>
									</span>
									<div class="form-line">
										<input type="text" class="form-control" name="item_price_end_sm" placeholder=" Admin Price (SM)" required>
									</div>
								</div>
								<div class="input-group">
									<span class="input-group-addon">
										<i class="material-icons">attach_money</i>
									</span>
									<div class="form-line">
										<input type="text" class="form-control" name="item_price_end_ss" placeholder=" Admin Price(SS)" required>
									</div>
								</div>
								
								<!-- dropship price -->
								<div class="input-group">
									<span class="input-group-addon">
										<i class="material-icons">attach_money</i>
									</span>
									<div class="form-line">
										<input type="text" class="form-control" name="item_price_dropship_sm" placeholder=" Dropship Price(SS)" required>
									</div>
								</div>
								<div class="input-group">
									<span class="input-group-addon">
										<i class="material-icons">attach_money</i>
									</span>
									<div class="form-line">
										<input type="text" class="form-control" name="item_price_dropship_ss" placeholder=" Dropship Price(SS)" required>
									</div>
								</div>

								<button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">REGISTER ITEM</button>

							</form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="../plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="../plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="../plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="../plugins/raphael/raphael.min.js"></script>
    <script src="../plugins/morrisjs/morris.js"></script>

    <!-- ChartJs -->
    <script src="../plugins/chartjs/Chart.bundle.js"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="../plugins/flot-charts/jquery.flot.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.time.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="../plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Custom Js -->
    <script src="../js/admin.js"></script>
    <script src="../js/pages/index.js"></script>

    <!-- Demo Js -->
    <script src="../js/demo.js"></script>
    
    		<script>
function addRow(PhoneCol) {
	var table = document.getElementById(PhoneCol);
	var rowCount = table.rows.length;
	if(rowCount < 3){                            // limit the user from creating fields more than your limits
		var row = table.insertRow(rowCount);
		var colCount = table.rows[0].cells.length;
		for(var i=0; i<colCount; i++) {
			var newcell = row.insertCell(i);
			newcell.innerHTML = table.rows[0].cells[i].innerHTML;
		}
	}else{
		alert("Maximum 3 number only!");
	}
}

</script>
<?php
					
		}
		mysqli_close($DBconnect);
	}
}
?>
</body>

</html>