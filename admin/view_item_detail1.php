<!DOCTYPE html>
<?php
session_start();
if($_SESSION['email']=="")
{
	header("location: index.html");
}
else
{
	include("inc_db.php");
	if($DBconnect!==FALSE);
	{
		$SQLquery="SELECT * FROM user WHERE email='$_SESSION[email]'";
		$QueryResult=mysqli_query($DBconnect, $SQLquery);
		
		while($row=mysqli_fetch_array($QueryResult))
		{
			$id=$row["no_id"];
			$email=$_SESSION['email'];
			$item_id=$_GET['item_id'];

?>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | LANEW Management System</title>
    <!-- Favicon-->
    <link rel="icon" href="../lanew_icon.jpeg" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="../plugins/node-waves/waves.css" rel="stylesheet" />
	
	<!-- Bootstrap Select Css -->
    <link href="../plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />
	
	<!-- Wait Me Css -->
    <link href="../plugins/waitme/waitMe.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="../plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="../css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../css/themes/all-themes.css" rel="stylesheet" />
    <style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>

<body class="theme-amber">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">LANEW ADMIN SITE</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <?php 
					
						$SQLquery6="SELECT * FROM verify_user WHERE sponsor_id='$id'";
						$QueryResult6=mysqli_query($DBconnect, $SQLquery6);
							
						while($row6=mysqli_fetch_array($QueryResult6))
						{
							if($row6['dp_image']=="")
							{
							?>
								<img src="../images/user.png" width="48" height="48" alt="User" />
							<?php
							}
							else
							echo '<img src="data:image/jpeg;base64,'.base64_encode( $row6['dp_image'] ).'" width="48" height="48"/>';
						}
					?>
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo "$id"; ?></div>
                    <div class="email"><?php echo "$email"; ?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="profile.php"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="view_order_list.php"><i class="material-icons">shopping_cart</i>Order List</a></li>
							<li><a href="view_withdraw_list.php"><i class="material-icons">account_balance_wallet</i>Withdraw List</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="logout.php"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
				
                    <?php include 'side_menu.php'; ?>
					
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    <img src="images/copy.jpg" alt="copy" style="width:270px;height:70px;">
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Order Item</h2>
            </div>
            <div class="row clearfix">
			<form name="order" id="sign_up" method="GET" action="save_order_yourself.php">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Order Form
                            </h2>
                        </div>
                        <div class="body">
						<?php
							$sql="SELECT * FROM item WHERE item_id='$item_id'";
							$query=mysqli_query($DBconnect, $sql);
							
							while($row=mysqli_fetch_row($query))
							{
								
								$sql2="SELECT * FROM price WHERE item_id='$item_id'";
								$query2=mysqli_query($DBconnect, $sql2);
								
								while($row2=mysqli_fetch_row($query2))
								{
						?>
                            
								<input type="hidden" name="sponsor_id" value="<?php echo $id; ?>">
								<input type="hidden" name="item_id" value="<?php echo $item_id; ?>">
								<input type="hidden" name="product_name" value="<?php echo $row[3]; ?>">
								<div class="input-group">
									<div>
									<center>
										<?php echo '<img src="data:image/jpeg;base64,'.base64_encode( $row[2] ).'" width="300" height="300"/>'; ?>
									</center>
									</br>
									</div>
								</div>
								<div class="input-group">
									<div>
										<b>Product Name</b> : <?php echo $row[3]; ?>
									</div>
								</div>
								<div class="input-group">
									<div>
										<b>Product Description</b> : <?php echo $row[4]; ?>
									</div>
								</div>
								<div class="input-group">
									<div>
										<b>Product Price</b> :
										
								
										<table>
										  <tr>
										    <th>AGENT PRICE</th>
										    <th>DROPSHIP PRICE</th>
										    <th>CUSTOMER HQ</th>
										  </tr>
										  
											<tr>
												<td><input name="item_price" type="radio" id="radio_1" class="with-gap radio-col-amber" value="<?php echo $row2[1]; ?>" />
												<label for="radio_1">RM<?php $sd1=$row2[1]; echo "$sd1";?></label> (Semenanjung Malaysia)</td>
												
												<td><input name="item_price" type="radio" id="radio_5" class="with-gap radio-col-amber" value="<?php echo $row2[5]; ?>" />
												<label for="radio_5">RM<?php $sd5=$row2[5]; echo "$sd5";?> </label> (Semenanjung Malaysia)</td>
												
												<td><input name="item_price" type="radio" id="radio_3" class="with-gap radio-col-amber" value="<?php echo $row2[3]; ?>" />
												<label for="radio_3">RM<?php $sd3=$row2[3]; echo "$sd3";?></label> (Semenanjung Malaysia)</td> 
												
											</tr>
										  
											<tr>  
												<td><input name="item_price" type="radio" id="radio_2" class="with-gap radio-col-amber" value="<?php echo $row2[2]; ?>" />
												<label for="radio_2">RM<?php $sd2=$row2[2]; echo "$sd2";?></label> (Sabah & Sarawak)</td>
										
												<td><input name="item_price" type="radio" id="radio_6" class="with-gap radio-col-amber" value="<?php echo $row2[6]; ?>" />
												<label for="radio_6">RM<?php $sd6=$row2[6]; echo "$sd6";?> </label> (Sabah & Sarawak)</td>
										
												<td><input name="item_price" type="radio" id="radio_4" class="with-gap radio-col-amber" value="<?php echo $row2[4]; ?>" />
												<label for="radio_4">RM<?php $sd4=$row2[4]; echo "$sd4";?> </label> (Sabah & Sarawak)</td>
											</tr>

											<!-- iqbalH - postage -->
											<?php
												$getPostRate = mysqli_query($DBconnect, "SELECT post_sm,post_smplus,post_ss,post_ssplus FROM postage WHERE id='1'", $DBconnect);
                                    		    if(mysqli_num_rows($getPostRate) != 0){
                                    		        while ($postValue = mysqli_fetch_assoc($getPostRate)) {
                                    		            $sm2pass = $postValue['post_sm'];
                                    		            $smplus2pass = $postValue['post_smplus'];
                                    		            $ss2pass = $postValue['post_ss'];
                                    		            $ssplus2pass = $postValue['post_ssplus'];                                                
                                    		        }
                                    		    }else{
                                    		        $sm2pass = '0.00';
                                    		        $smplus2pass = '0.00';                                                
                                    		        $ss2pass = '0.00';
                                    		        $ssplus2pass = '0.00';
                                    		    }
                                    		?>
											<tr>
												<th class="align-center" colspan="3">Postage Rate</th>
											</tr>
											<tr>
												<th>Location</th>
												<th>First Item</th>
												<th>Next Item</th>
											</tr>
											<tr>												
												<th>Semenanjung Malaysia</th>												
												<th>RM <?php echo $sm2pass; ?></th>												
												<th>RM <?php echo $smplus2pass; ?></th>
											</tr>
											<tr>
												<th>Sabah & Sarawak</th>												
												<th>RM <?php echo $ss2pass; ?></th>												
												<th>RM <?php echo $ssplus2pass; ?></th>
											</tr>
											<!-- iqbalH - postage -->									
									
									</table>
									
									</div>
								</div>
								<div class="input-group">
									<div>
										<b>Available Quantity</b> : <?php echo $row[5]; ?> Units
									</div>
								</div>
								<div class="input-group">
									<span class="input-group-addon">
										<i class="material-icons">add</i>
									</span>
									<div class="form-line">
										<input type="text" class="form-control" name="order_quantity" id="order_quantity" placeholder="Order Quantity" required>
									</div>
								</div>								

						<?php
								}
							}
							
						?>
                        </div>
                    </div>
                </div>
				
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Order Shipping Form
                            </h2>
                        </div>
                        <div class="body">		
							<div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">person</i>
								</span>
								<div class="form-line">
									<input type="text" class="form-control" name="recipient_name" placeholder="Recipient Name" style="text-transform: capitalize;" required>
								</div>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">call</i>
								</span>
								<div class="form-line">
									<input type="text" class="form-control" name="recipient_tel_no" placeholder="Recipient Telephone Number" style="text-transform: capitalize;" required>
								</div>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">place</i>
								</span>
								<div class="form-line">
									<input type="text" class="form-control" name="recipient_address1" placeholder="Address Line 1" style="text-transform: capitalize;" required>
								</div>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">place</i>
								</span>
								<div class="form-line">
									<input type="text" class="form-control" name="recipient_address2" placeholder="Address Line 2" style="text-transform: capitalize;" required>
								</div>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">place</i>
								</span>
								<div class="form-line">
									<input type="text" class="form-control" name="recipient_postcode" placeholder="Postcode" style="text-transform: capitalize;" required>
								</div>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">place</i>
								</span>
								<div class="form-line">
									<input type="text" class="form-control" name="recipient_city" placeholder="Recipient City" style="text-transform: capitalize;" required>
								</div>
							</div>
							<div class="input-group">
									<span class="input-group-addon">
										<i class="material-icons">place</i>
									</span>
									<div class="form-line">
									<!-- on change select radio, reset this -->
											<select name="state" id="state" required>
												<option value="">-- Please Select State --</option>
												<option value="johor">Johor</option>
												<option value="kedah">Kedah</option>
												<option value="kelantan">Kelantan</option>
												<option value="kuala_lumpur">Kuala Lumpur</option>
												<option value="labuan">Labuan</option>
												<option value="melaka">Melaka</option>
												<option value="negeri">Negeri</option>
												<option value="pahang">Pahang</option>
												<option value="penang">Penang</option>
												<option value="perak">Perak</option>
												<option value="perlis">Perlis</option>
												<option value="putrajaya">Putrajaya</option>
												<option value="selangor">Selangor</option>
												<option value="sabah">Sabah</option>
												<option value="sarawak">Sarawak</option>
												<option value="terengganu">Terengganu</option>
											</select>
									</div>
								</div>
								
								<!-- iqbalH - postage calculation -->
								<div class="input-group">
									<span class="input-group-addon">
										<i class="material-icons">attach_money</i>
									</span>
									<label for="totalPrice">Total Payment:</label>
									<div class="form-line">
										<input type="text" class="form-control" name="totalPrice" id="totalPrice" placeholder="RM 0.00">
									</div>
									<label>To get correct value display, always <b class="col-red">RESELECT STATE</b> after changing <b class="col-red">PRODUCT & QUANTITY</b> value</label class="col-red">
									<!-- <label><b class="col-red">NOT WORKING for bulk itme type</b></label> -->
								</div>
								<!-- iqbalH - postage calculation -->
                        </div>
                    </div>
                </div>
				<button class="btn btn-block btn-lg bg-green waves-effect" type="submit">ORDER ITEM</button>
			</form></br></br>
            </div>

        </div>
	</section>

    <!-- Jquery Core Js -->
	<script src="../plugins/jquery/jquery.min.js"></script>

	<!-- iqbalH - postage -->	
	<!-- auto calculation for total price to pay display -->
	<script type="text/javascript">
	$(document).ready(function(){
		var finalPrice = 0;
		var ssPrice = <?php echo $ss2pass; ?>;
		var ssPlusPrice = <?php echo $ssplus2pass; ?>;
		var smPrice = <?php echo $sm2pass; ?>;
		var smPlusPrice = <?php echo $smplus2pass; ?>;

		//to get bulk status
		//side note. lazy to make the variable stand-alone, just echo here
		<?php
		$getBulk = "SELECT bulk FROM item WHERE item_id='$item_id'";
		$queryBulk = mysqli_query($DBconnect, $getBulk);
		if(mysqli_num_rows($queryBulk) != 0){
			if($bulkStatus = mysqli_fetch_object($queryBulk)) {
				echo 'var bulkStatus = "'.$bulkStatus->bulk.'";';
			}
		}
		//get postage fee inclusion
		$getCurrent="SELECT postage_enable FROM item WHERE item_id='$item_id'";
    	$queryCurrent=mysqli_query($DBconnect, $getCurrent);
    	if($resultCurrent = mysqli_fetch_object($queryCurrent)){
    	    echo 'var freePost = "'.$resultCurrent->postage_enable.'";';
    	}
		?>
		
		//RESET THE SELECT?
		$("input[name=item_price]").on('change', function(){
			$('#state').val('');
		});
		$("#order_quantity").on('change', function(){
			$('#state').val('');
		})
		
		//begin calculation, on select state from dropdown
	    $('#state').on('change', function() {
			var productPrice = parseInt($("input[name=item_price]:checked").val());
			var totalItem = $("#order_quantity").val();
    		var state=$("#state option:selected").val();

			if(freePost === "no"){
				if(bulkStatus === "nay"){
					//check order quantity
					if($.isNumeric(totalItem) && $.isNumeric(productPrice)){
						if(totalItem !== "1"){
							var nextItem = totalItem - 1;
							if(state !== 'sarawak' && state !=='sabah' && state !=='labuan'){
								finalPrice = ((nextItem * smPlusPrice) + smPrice + (productPrice * totalItem));
							}else{
								finalPrice = ((nextItem * ssPlusPrice) + ssPrice + (productPrice * totalItem));
							}
						}
						else{
							if(state !== 'sarawak' && state !=='sabah' && state !=='labuan'){
								finalPrice = smPrice + productPrice;
							}else{
								finalPrice = ssPrice + productPrice;
							}
						}
					}
				}else{
					if(state !== 'sarawak' && state !=='sabah' && state !=='labuan'){
						finalPrice = smPrice + (productPrice * totalItem);
					}else{
						finalPrice = ssPrice + (productPrice * totalItem);
					}
				}
			}else{
				finalPrice = productPrice * totalItem
			}

			//display the final price
	        if ($.isNumeric(finalPrice)){
	            $("#totalPrice").val("RM "+finalPrice);
	        }
	    });
	})
	</script>
	<!-- iqbalH - postage -->

    <!-- Bootstrap Core Js -->
    <script src="../plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="../plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="../plugins/raphael/raphael.min.js"></script>
    <script src="../plugins/morrisjs/morris.js"></script>

    <!-- ChartJs -->
    <script src="../plugins/chartjs/Chart.bundle.js"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="../plugins/flot-charts/jquery.flot.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.time.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="../plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Custom Js -->
    <script src="../js/admin.js"></script>
    <script src="../js/pages/index.js"></script>
	<script src="../js/pages/forms/basic-form-elements.js"></script>

    <!-- Demo Js -->
	<script src="../js/demo.js"></script>
	
<?php
					
		}
		mysql_close($DBconnect);
	}
}
?>
</body>

</html>