<div class="table-responsive">

	<!-- <b>Item Total Order</b> : &nbsp; -->
	
	<?php
	
		$start=date("Y-m-01");
		$end=date("Y-m-t");
		$totalSale=0;
	
		$SQLquery3="SELECT SUM(order_quantity) AS quantity_total,
					SUM(order_total) AS total
					FROM order_list
					WHERE order_status='Confirm'
					AND order_date>='$start'
					AND order_date<='$end'";
		$QueryResult3=mysql_query($SQLquery3, $DBconnect);
		
		while(($row3=mysql_fetch_array($QueryResult3))!==FALSE)
		{
			$GLOBALS['quantity_total']=$row3["quantity_total"];
			$GLOBALS['total']=$row3["total"];
			
			//echo "$quantity_total";
		
	
	?>
	<!-- Units 
	
	</br>-->
	
	<!-- <b>Gross Profit</b> : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RM -->
	
	<?php
		
			$GLOBALS['gross']=$total-($quantity_total*17);// this is the hardcoded value
			
			// echo "$gross";

		}
	
	?>

	<b>Order By Registered ID</b>
	</br></br>
    <table class="table table-hover dashboard-task-infos">
        <thead>
            <tr>
                <th>No</th>
                <th>Agent ID</th>
                <th>Dropship ID</th>
				<th>Order ID</th>
				<th>Order Quantity</th>
				<th>Order Total</th>
            </tr>
        </thead>
									
		<?php
										
		$SQLquery2="SELECT *, GROUP_CONCAT(order_id SEPARATOR '<br>') AS Group1, 
								GROUP_CONCAT(order_quantity SEPARATOR ' Units<br>') AS Group2, 
								GROUP_CONCAT(CONCAT('RM ',order_total) SEPARATOR '<br>') AS Group3,
								SUM(order_quantity) AS Group4,
								SUM(order_total) AS Group5
					FROM order_list 
					WHERE order_status='Confirm'
					AND order_date>='$start'
					AND order_date<='$end'
					GROUP BY sponsor_id";
		$QueryResult2=mysql_query($SQLquery2, $DBconnect);
											
		$tempNum=1;	$sumQ2 =0;
										
		while(($row2=mysql_fetch_array($QueryResult2))!==FALSE)
		{
			$sponsor_id=$row2['sponsor_id'];
			$order_id=$row2['Group1'];
			$order_quantity=$row2['Group2'];
			$order_total=$row2['Group3'];
			$sumQ=$row2['Group4'];
			$sumT=$row2['Group5'];								
		?>
		<tbody>
            <tr>
				<td><?php echo "$tempNum"; ?></td>
				<td><?php echo "$sponsor_id"; ?></td>
				<td>Self Order</td>
				<td><?php echo "$order_id"; ?></td>
                <td><?php echo "$order_quantity"; ?> Units</td>
				<td><?php echo "$order_total"; ?></td>

            </tr>
			<?php
			// <!-- iqbalH - Get Dropship Order by Sponser ID -->
			$getDropship = "SELECT dropship_id FROM reg_dropship WHERE id_sponsor='$sponsor_id'";
			$queryDropship = mysql_query($getDropship, $DBconnect);
			while($resultDropship = mysql_fetch_array($queryDropship)){
				$dropshipID = $resultDropship['dropship_id'];
				unset($dropId);
				unset($dropOrder);
				unset($dropTotal);

				$getDropshipOrder = "SELECT * FROM dropship_order WHERE dropship_id='$dropshipID' AND order_status='Confirm'
									AND order_date>='$start' AND order_date<='$end'";
				$queryDropshipOrder = mysql_query($getDropshipOrder, $DBconnect);
				if(mysql_num_rows($queryDropshipOrder) != 0){
					while($resultDropshipOrder = mysql_fetch_array($queryDropshipOrder)){
						$dropId[] = $resultDropshipOrder['order_id'].'<br>';

            			$dropOrder[] = $resultDropshipOrder['order_quantity'].' Units<br>';
						$sumQ += $resultDropshipOrder['order_quantity'];

						$dropTotal[] = 'RM '.number_format((float)$resultDropshipOrder['order_total'], 2, '.', '').'<br>';
						$sumT += $resultDropshipOrder['order_total'];
					}
			?>
			<tr>
				<td></td>
				<td></td>
				<td><?php echo $dropshipID; ?></td>
				<td>
				<?php 
				for ($i=0; $i < count($dropId) ; $i++) { 
					echo $dropId[$i];
				} 
				?>
				</td>
				<td>
				<?php 
				for ($i=0; $i < count($dropOrder) ; $i++) { 
					echo $dropOrder[$i];
				} 
				?>
				</td>
				<td>
				<?php 
				for ($i=0; $i < count($dropTotal) ; $i++) { 
					echo $dropTotal[$i];
				} 
				?>
				</td>
			</tr>
			<?php
				}
			}
			// <!-- iqbalH - Get Dropship Order by Sponser ID -->
			?>
			<tr>
				<td><b>Total</b></td>
				<td></td>
				<td></td>
				<td></td>
				<td><b><?php echo "$sumQ"; ?> Units</b></td>
				<td><b>RM <?php echo number_format((float)$sumT, 2, '.', ''); ?></b></td>
			</tr>
		<?php
			$tempNum++;
			$sumQ2 += $sumQ;
			$totalSale += number_format((float)$sumT, 2, '.', '');
		}
									
		?>
		</tbody>
    </table>
	<p class="font-16"><b>Total Sales for Current Month (<?php echo date('F Y', strtotime($start))?>): </b> RM <?php echo number_format((float)$totalSale, 2, '.', ''); ?></p>

	<!-- iqbalH - GROSS PROFIT CALCULATION -->
	</br></br>
	<b>Gross Profit</b>
	</br></br>
	<b>Total Order Unit for ALL Product:</b>  <?php echo "$sumQ2" ?> Units
	</br>
	<b>Calculation Schema:</b>
	</br>
	Gross = Order Total - (Total Order Unit * Cost of Good)
	</br></br>
	
	<table class="table table-condensed table-striped">
		<thead>
			<th>PRODUCT</th>
			<th>ORDER TOTAL</th>
			<th>COST OF GOODS</th>
			<th>TOTAL ORDER UNIT</th>
			<th>GROSS PROFIT</th>
		</thead>	  
		<tr>
		<!-- get product id -->
		<?php
			$totalGross = 0;
			$getProduct = "SELECT item_id,item_name FROM item";
			$queryProduct = mysql_query($getProduct,$DBconnect);
			if(mysql_num_rows($queryProduct) != 0){
				while($resultProduct = mysql_fetch_assoc($queryProduct)){
					$gross = 0;
					$totalQuantity =0;
					$totalOrder =0;

					$itemID = $resultProduct["item_id"];

					//get order quantity, order total for agent order
					$getOrder="SELECT order_quantity,order_total FROM order_list 
						WHERE order_status='Confirm' AND item_id='$itemID' AND order_date>='$start' AND order_date<='$end'";
					$queryOrder = mysql_query($getOrder, $DBconnect);;
					if(mysql_num_rows($queryOrder) != 0){
						while($resultOrder = mysql_fetch_assoc($queryOrder)) {
							$totalQuantity += $resultOrder['order_quantity'];
							$totalOrder += $resultOrder['order_total'];
						}
					}
					else{
						$totalQuantity += 0;
						$totalOrder += 0;;
					}

					//get order quantity, order total for dropship order
					$getOrder="SELECT order_quantity,order_total FROM dropship_order 
						WHERE order_status='Confirm' AND item_id='$itemID' AND order_date>='$start' AND order_date<='$end'";
					$queryOrder = mysql_query($getOrder, $DBconnect);;
					if(mysql_num_rows($queryOrder) != 0){
						while($resultOrder = mysql_fetch_assoc($queryOrder)) {
							$totalQuantity += $resultOrder['order_quantity'];
							$totalOrder += $resultOrder['order_total'];
						}
					}
					else{
						$totalQuantity += 0;
						$totalOrder += 0;;
					}

					//get cost of good
					$getCostGood = "SELECT goods_cost FROM price WHERE item_id='$itemID'";
					$queryCostGood = mysql_query($getCostGood,$DBconnect);
					if($resultCostGood = mysql_fetch_object($queryCostGood)){
						$goodsCost = $resultCostGood->goods_cost;
					}else{
						$goodsCost = "0";
					}

					//calculate
					$gross = $totalOrder - ($totalQuantity * $goodsCost);
					$gross = number_format((float)$gross, 2, '.', '');
					
					echo '<tr>';
					echo '<td hidden>'.$itemID.'</td>';
					echo '<td>'.$resultProduct["item_name"].'</td>';
					echo '<td>RM '.$totalOrder.'</td>';
					echo '<td>RM '.$goodsCost.'</td>';
					echo '<td>'.$totalQuantity.' Units</td>';
					echo '<td>RM '.$gross.'</td>';
					echo '</tr>';

					$totalGross += $gross;
				}
			}
		?>
		</tr>
		<tfoot>
			<th colspan='3'></th>
			<th>TOTAL GROSS PROFIT</th>
			<!-- calculate -->
			<th>RM <?php echo number_format((float)$totalGross, 2, '.', '');?></th>
		</tfoot>							

	</table>
	<!-- iqbalH - GROSS PROFIT CALCULATION -->	
	
</div>