<!DOCTYPE html>
	<?php
session_start();
if($_SESSION['email']=="")
{
	header("location: index.html");
}
else
{
	include("inc_db.php");
	if($DBconnect!==FALSE);
	{
		$SQLquery="SELECT * FROM user WHERE email='$_SESSION[email]'";
		$QueryResult=mysqli_query($DBconnect, $SQLquery);
		
		while($row=mysqli_fetch_array($QueryResult))
		{
			$id=$row["no_id"];
			$_SESSION['id']=$row["no_id"];
			$email=$row["email"];
?>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | LANEW Management System</title>
    <!-- Favicon-->
    <link rel="icon" href="../lanew_icon.jpeg" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="../plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="../plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="../css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../css/themes/all-themes.css" rel="stylesheet" />
	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script><!-- jQuery Library-->
	<link rel="stylesheet" href="passwordscheck.css" /><!-- Include Your CSS file here-->
	<script src="passwordscheck.js"></script><!-- Include Your jQUery file here-->
	<script src="jquery.min.js"></script>
	<script type="text/javascript">
			$(document).ready(function () {
				//Disable cut copy paste
				$('body').bind('cut copy paste', function (e) {
					e.preventDefault();
				});
			   
				//Disable mouse right click
				$("body").on("contextmenu",function(e){
					return false;
				});
			});
			
			function checkCurrent()
			{
			 var name=document.getElementById( "sponsor_id" ).value;
				
			 if(name)
			 {
			  $.ajax({
			  type: 'get',
			  url: 'checkdata.php',
			  data: {
			   sponsor_id:name,
			  },
			  success: function (response) {
			   $( '#name_status' ).html(response);
			   if(response=="OK")	
			   {
				return true;	
			   }
			   else
			   {
				return false;	
			   }
			  }
			  });
			 }
			 else
			 {
			  $( '#name_status' ).html("");
			  return false;
			 }
			}
			
			function checkall()
			{
			 var namehtml=document.getElementById("name_status").innerHTML;

			 if((namehtml)=="OK")
			 {
			  return true;
			 }
			 else
			 {
			  return false;
			 }
			}
		</script>
</head>

<body class="theme-amber">
    
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">LANEW</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <?php 
					
						$SQLquery6="SELECT * FROM verify_user WHERE sponsor_id='$id'";
						$QueryResult6=mysqli_query($DBconnect, $SQLquery6);
							
						while($row6=mysqli_fetch_array($QueryResult6))
						{
							if($row6['dp_image']=="")
							{
							?>
								<img src="../images/user.png" width="48" height="48" alt="User" />
							<?php
							}
							else
							echo '<img src="data:image/jpeg;base64,'.base64_encode( $row6['dp_image'] ).'" width="48" height="48"/>';
						}
					?>
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo "$id"; ?></div>
                    <div class="email"><?php echo "$email"; ?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="profile.php"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="view_sponsor.php"><i class="material-icons">group</i>Sponsor</a></li>
                            <li><a href="view_order_list.php"><i class="material-icons">shopping_cart</i>Order List</a></li>
							<li><a href="view_withdraw_list.php"><i class="material-icons">account_balance_wallet</i>Withdraw List</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="logout.php"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
				
                    <?php include 'side_menu.php'; ?>
					
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    <img src="../images/copy.jpg" alt="copy" style="width:270px;height:70px;">
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
            <div class="row clearfix">
			<!-- Colorful Panel Items With Icon -->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                AGENT LIST
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group" id="accordion_17" role="tablist" aria-multiselectable="true">
									
									<div class="panel panel-col-pink">
                                            <div class="panel-heading" role="tab" id="headingOne_17">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion_17" href="#collapseOne_179" aria-expanded="true" aria-controls="collapseOne_17">
                                                        <i class="material-icons">perm_contact_calendar</i> ALL AGENT REGISTERED
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne_179" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_17">
                                                <div class="panel-body" >
                                                    <div class="table-responsive">
														<table class="table table-hover dashboard-task-infos">
															<thead>
																<tr>
																	<form name="search" method="get" action="view_sponsor_profile.php">
																	<td colspan='3'>
																		<div class="input-group">
																			<span class="input-group-addon">
																				<i class="material-icons">search</i>
																			</span>
																			<div>
																				<input type="text" id="sponsor_id" class="form-control" name="user_id" placeholder="AGENT ID" onkeyup="checkCurrent();" required autofocus> 
																				SEARCH AGENT : <span id="name_status"></span>
																			</div>
																		</div>
																	</td>
																	<td>
																		<button type="submit" class="btn bg-green waves-effect">SEARCH AGENT</button>
																	</td>
																	</form>
																</tr>
																<tr>
																	<th>Agent ID</th>
																	<th>Name</th>
																	<th>Date Register</th>
																	<th>Status</th>
																	<th>Action</th>
																</tr>
															</thead>
															
															<?php
									
																$SQLquery33="SELECT * FROM user ORDER BY no DESC";
																$QueryResult33=mysqli_query($DBconnect, $SQLquery33);
																	
																while($row33=mysqli_fetch_array($QueryResult33))
																{
																	$user_status=$row33["user_status"];
														
															?>
															<tbody>
																<tr>
																	<td><?php echo "$row33[no_id]"; ?></td>
																	<td><?php echo "$row33[nama]"; ?></td>
																	<td><?php echo "$row33[register_time]"; ?></td>
																	
																	<td>
																	<?php 
																		if($user_status=="Active")
																		{
																			?>
																			<span class="label bg-green">Active</span>
																			<?php
																		}
																		else if($user_status=="Suspended")
																		{
																			?>
																			<span class="label bg-red">Suspended</span>
																			<?php
																		}
																	?>
																	</td>
																	<td>
																		<form method="get" action="view_sponsor_profile.php">
																			<input type="hidden" name="user_id" value="<?php echo "$row33[no_id]"; ?>">
																			<button type="submit" class="btn bg-blue waves-effect btn-xs">View Sponsor Profile</button>
																		</form>
																		&nbsp;
																		<form method="get" action="edit_sponsor.php">
																			<input type="hidden" name="user_id" value="<?php echo "$row33[no_id]"; ?>">
																			<button type="submit" class="btn bg-green waves-effect btn-xs">Edit Sponsor Profile</button>
																		</form>
																	</td>
																</tr>
															</tbody>
															<?php
																	
																}
															
															?>
														</table>
													</div>
                                                </div>
                                            </div>
                                        </div>
									
                                        <div class="panel panel-col-pink">
                                            <div class="panel-heading" role="tab" id="headingOne_17">
                                                <h4 class="panel-title">
                                                    <a  class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_17" href="#collapseOne_17" aria-expanded="true" aria-controls="collapseOne_17">
                                                        <i class="material-icons">perm_contact_calendar</i> AGENT UNDER YOURSELF
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne_17" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne_17">
                                                <div class="panel-body">
                                                    <div class="table-responsive">
														<table class="table table-hover dashboard-task-infos">
															<thead>
																<tr>
																	<th>Agent ID</th>
																	<th>Name</th>
																	<th>Status</th>
																	<th>Action</th>
																</tr>
															</thead>
															
															<?php
									
																$SQLquery22="SELECT * FROM sponsor WHERE id_sponsor='$id'";
																$QueryResult22=mysqli_query($DBconnect, $SQLquery22);
																	
																while($row22=mysqli_fetch_array($QueryResult22))
																{
																	$reg_id=$row22["id_reg"];
																	
																	$SQLquery3="SELECT * FROM user WHERE no_id='$reg_id'";
																	$QueryResult3=mysqli_query($DBconnect, $SQLquery3);
																		
																	while($row3=mysqli_fetch_array($QueryResult3))
																	{
																		$nama1=$row3["nama"];
																		$user_status=$row3["user_status"];
														
															?>
															<tbody>
																<tr>
																	<td><?php echo "$reg_id"; ?></td>
																	<td><?php echo "$nama1"; ?></td>
																	<td>
																	<?php 
																		if($user_status=="Active")
																		{
																			?>
																			<span class="label bg-green">Active</span>
																			<?php
																		}
																		else if($user_status=="Suspended")
																		{
																			?>
																			<span class="label bg-red">Suspended</span>
																			<?php
																		}
																	?>
																	</td>
																	<td>
																		<form method="get" action="view_sponsor_profile.php">
																			<input type="hidden" name="user_id" value="<?php echo "$reg_id"; ?>">
																			<button type="submit" class="btn bg-blue waves-effect btn-xs">View Sponsor Profile</button>
																		</form>
																		&nbsp;
																		<form method="get" action="edit_sponsor.php">
																			<input type="hidden" name="user_id" value="<?php echo "$reg_id"; ?>">
																			<button type="submit" class="btn bg-green waves-effect btn-xs">Edit Sponsor Profile</button>
																		</form>
																	</td>
																</tr>
															</tbody>
															<?php
																	}
																}
															
															?>
														</table>
													</div>
                                                </div>
                                            </div>
                                        </div>
										
                                        <div class="panel panel-col-cyan">
                                            <div class="panel-heading" role="tab" id="headingTwo_17">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_17" href="#collapseTwo_17" aria-expanded="false"
                                                       aria-controls="collapseTwo_17">
                                                        <i class="material-icons">cloud_download</i> LEADER LIST WITH AGENT
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo_17" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_17">
                                                <div class="panel-body">
                                                    <div class="table-responsive">
														<table class="table table-hover dashboard-task-infos">
															<thead>
																<tr>
																	<th>Agent ID</th>
																	<th>Name</th>
																	<th>Status</th>
																	<th>Registed Agent</th>
																	<th>Action</th>
																</tr>
															</thead>
															
															<?php
									
																$SQLquery23="SELECT * FROM user WHERE level='Leader'";
																$QueryResult23=mysqli_query($DBconnect, $SQLquery23);
																	
																while($row23=mysqli_fetch_array($QueryResult23))
																{
																	$sponsor_id=$row23["no_id"];
																	$user_status=$row23["user_status"];
																	
																	$sql = ("SELECT COUNT(id_sponsor) FROM sponsor WHERE id_sponsor='$sponsor_id'");
																	$rs = mysqli_query($DBconnect, $sql);
																	 //-----------^  need to run query here

																	$result = mysqli_fetch_array($rs);
																	 //here you can echo the result of query
																	
															?>
															<tbody>
																<tr>
																	<td><?php echo "$sponsor_id"; ?></td>
																	<td><?php echo "$row23[nama]"; ?></td>
																	<td>
																	<?php 
																		if($user_status=="Active")
																		{
																			?>
																			<span class="label bg-green">Active</span>
																			<?php
																		}
																		else if($user_status=="Suspended")
																		{
																			?>
																			<span class="label bg-red">Suspended</span>
																			<?php
																		}
																	?>
																	</td>
																	<td><?php echo $result[0]; ?></td>
																	<td>
																		<form method="get" action="view_leader_agent.php">
																			<input type="hidden" name="sponsor_id" value="<?php echo "$sponsor_id"; ?>">
																			<input type="hidden" name="nama" value="<?php echo "$row23[nama]"; ?>">
																			<button type="submit" class="btn bg-blue waves-effect btn-xs">View Leader Agent</button>
																		</form>
																	</td>
																</tr>
															</tbody>
															<?php
																	
																}
															
															?>
														</table>
													</div>
                                                </div>
                                            </div>
                                        </div>
										
										<div class="panel panel-col-cyan">
                                            <div class="panel-heading" role="tab" id="collapseTwo_17">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_17" href="#collapseTwo_171" aria-expanded="false"
                                                       aria-controls="collapseTwo_17">
                                                        <i class="material-icons">cloud_download</i> VERIFICATION AGENT LIST REQUEST
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo_171" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseTwo_17">
                                                <div class="panel-body">
                                                    <div class="table-responsive">
														<table class="table table-hover dashboard-task-infos">
															<thead>
																<tr>
																	<th>Agent ID</th>
																	<th>Name</th>
																	<th>Status</th>
																	<th>Action</th>
																</tr>
															</thead>
															
															<?php
									
																$SQLquery24="SELECT * FROM verify_user WHERE status_reg='NEW' AND verify_status='NOT VERIFY'";
																$QueryResult24=mysqli_query($DBconnect, $SQLquery24);
																	
																while($row24=mysqli_fetch_array($QueryResult24))
																{
																	$sponsor_id1=$row24["sponsor_id"];
																	
																	$SQLquery21="SELECT * FROM user WHERE no_id='$sponsor_id1'";
																	$QueryResult21=mysqli_query($DBconnect, $SQLquery21);
																		
																	while($row21=mysqli_fetch_array($QueryResult21))
																	{
																	
															?>
															<tbody>
																<tr>
																	<td><?php echo "$sponsor_id1"; ?></td>
																	<td><?php echo "$row21[nama]"; ?></td>
																	<td>
																	<?php 
																		if($user_status=="Active")
																		{
																			?>
																			<span class="label bg-green">Active</span>
																			<?php
																		}
																		else if($user_status=="Suspended")
																		{
																			?>
																			<span class="label bg-red">Suspended</span>
																			<?php
																		}
																	?>
																	</td>
																	<td>
																		<form method="get" action="view_verify_detail.php">
																			<input type="hidden" name="sponsor_id" value="<?php echo "$sponsor_id1"; ?>">
																			<button type="submit" class="btn bg-blue waves-effect btn-xs">View Verify Detail</button>
																		</form>
																	</td>
																</tr>
															</tbody>
															<?php
																	}
																}
															
															?>
														</table>
													</div>
                                                </div>
                                            </div>
                                        </div>
										
										<div class="panel panel-col-cyan">
                                            <div class="panel-heading" role="tab" id="collapseTwo_17">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_17" href="#collapseTwo_172" aria-expanded="false"
                                                       aria-controls="collapseTwo_17">
                                                        <i class="material-icons">cloud_download</i> VERIFICATION AGENT LIST
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo_172" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseTwo_17">
                                                <div class="panel-body">
                                                    <div class="table-responsive">
														<table class="table table-hover dashboard-task-infos">
															<thead>
																<tr>
																	<th>Agent ID</th>
																	<th>Name</th>
																	<th>Status</th>
																	<th>Action</th>
																</tr>
															</thead>
															
															<?php
									
																$SQLquery241="SELECT * FROM verify_user";
																$QueryResult241=mysqli_query($DBconnect, $SQLquery241);
																	
																while($row241=mysqli_fetch_array($QueryResult241))
																{
																	$sponsor_id11=$row241["sponsor_id"];
																	$verify_status=$row241["verify_status"];
																	
																	$SQLquery211="SELECT * FROM user WHERE no_id='$sponsor_id11'";
																	$QueryResult211=mysqli_query($DBconnect, $SQLquery211);
																		
																	while($row211=mysqli_fetch_array($QueryResult211))
																	{
																	
															?>
															<tbody>
																<tr>
																	<td><?php echo "$sponsor_id11"; ?></td>
																	<td><?php echo "$row211[nama]"; ?></td>
																	<td>
																	<?php 
																		if($verify_status=="VERIFY")
																		{
																			?>
																			<span class="label bg-green">VERIFY</span>
																			<?php
																		}
																		else if($verify_status=="NOT VERIFY")
																		{
																			?>
																			<span class="label bg-red">NOT VERIFY</span>
																			<?php
																		}
																	?>
																	</td>
																	<td>
																		<form method="get" action="view_verify.php">
																			<input type="hidden" name="sponsor_id" value="<?php echo "$sponsor_id11"; ?>">
																			<button type="submit" class="btn bg-blue waves-effect btn-xs">View Agent Detail</button>
																		</form>
																	</td>
																</tr>
															</tbody>
															<?php
																	}
																}
															
															?>
														</table>
													</div>
                                                </div>
                                            </div>
                                        </div>
										
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="../plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="../plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="../plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="../plugins/raphael/raphael.min.js"></script>
    <script src="../plugins/morrisjs/morris.js"></script>

    <!-- ChartJs -->
    <script src="../plugins/chartjs/Chart.bundle.js"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="../plugins/flot-charts/jquery.flot.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.time.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="../plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Custom Js -->
    <script src="../js/admin.js"></script>
    <script src="../js/pages/index.js"></script>

    <!-- Demo Js -->
    <script src="../js/demo.js"></script>
<?php
					
		}
		mysqli_close($DBconnect);
	}
}
?>
</body>

</html>