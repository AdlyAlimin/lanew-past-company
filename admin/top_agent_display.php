<div class="card">
                        <div class="header">
                            <h2>Top Agent Sales <?php 
											
													$bulan=date("n");
											
													if($bulan==1)
														echo "January";
													else if($bulan==2)
														echo "February";
													else if($bulan==3)
														echo "March";
													else if($bulan==4)
														echo "April";
													else if($bulan==5)
														echo "May";
													else if($bulan==6)
														echo "June";
													else if($bulan==7)
														echo "July";
													else if($bulan==8)
														echo "August";
													else if($bulan==9)
														echo "September";
													else if($bulan==10)
														echo "October";
													else if($bulan==11)
														echo "November";
													else if($bulan==12)
														echo "December";
						
												?> 
								2018</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
                                            <th>No</th>
											<th>Name</th>
                                            <th>Quantity</th>
											<th>Total Sales </th>
                                        </tr>
                                    </thead>
									
									<?php
									
										$start=date("Y-m-01");
										$end=date("Y-m-t");
										
										$SQLquery5="SELECT SUM(order_list.order_quantity) AS quantity,
													SUM(order_list.order_total) AS total, order_list.sponsor_id, user.*
													FROM order_list, user
													WHERE order_list.sponsor_id=user.no_id
													AND order_list.order_status='Confirm'
													AND order_date>='$start' 
													AND order_date<='$end'
													GROUP BY order_list.sponsor_id
													ORDER BY quantity DESC
													LIMIT 10";
										$QueryResult5=mysqli_query($DBconnect, $SQLquery5);
											
										$tempNum=1;
										
										while($row5=mysqli_fetch_assoc($QueryResult5))
										{
											$quantity=$row5["quantity"];
											$total=$row5["total"];
											$nama=$row5["nama"];
											
											if(strpos($row5["sponsor_id"], 'LA9999') === false){
								
									?>
									<tbody>
                                        <tr>
											<td><?php echo "$tempNum"; ?></td>
											<td><?php echo "$nama"; ?></td>
											<td><?php echo "$quantity"; ?></td>
											<td>RM<?php echo "$total"; ?></td>
                                        </tr>
                                    </tbody>
									<?php
											$tempNum++;
											}
									}
									?>
                                </table>
                            </div>
                        </div>
                    </div>