<!DOCTYPE html>
<?php
session_start();
if($_SESSION['email']=="")
{
	header("location: index.html");
}
else
{
	include("inc_db.php");
	if($DBconnect!==FALSE);
	{
		$SQLquery="SELECT * FROM user WHERE email='$_SESSION[email]'";
		$QueryResult=mysqli_query($DBconnect, $SQLquery);
		
		while($row=mysqli_fetch_array($QueryResult))
		{
			$id=$row["no_id"];
			$email=$_SESSION['email'];
			$item_id=$_GET['item_id'];
?>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | LANEW Management System</title>
    <!-- Favicon-->
    <link rel="icon" href="../lanew_icon.jpeg" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="../plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="../plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="../css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../css/themes/all-themes.css" rel="stylesheet" />
    <style>
/* table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
} */
</style>
</head>

<body class="theme-amber">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">LANEW ADMIN SITE</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <?php 
					
						$SQLquery61="SELECT * FROM verify_user WHERE sponsor_id='$id'";
						$QueryResult61=mysqli_query($DBconnect, $SQLquery61);
							
						while($row61=mysqli_fetch_array($QueryResult61))
						{
							if($row61['dp_image']=="")
							{
							?>
								<img src="../images/user.png" width="48" height="48" alt="User" />
							<?php
							}
							else
							echo '<img src="data:image/jpeg;base64,'.base64_encode( $row61['dp_image'] ).'" width="48" height="48"/>';
						}
					?>
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo "$id"; ?></div>
                    <div class="email"><?php echo "$email"; ?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="profile.php"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="view_order_list.php"><i class="material-icons">shopping_cart</i>Order List</a></li>
							<li><a href="view_withdraw_list.php"><i class="material-icons">account_balance_wallet</i>Withdraw List</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="logout.php"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
				
                    <?php include 'side_menu.php'; ?>
					
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    <img src="images/copy.jpg" alt="copy" style="width:270px;height:70px;">
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>View Item</h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                View Item
                            </h2>
                        </div>
                        <div class="body">
						<?php
							$sql="SELECT * FROM item WHERE item_id='$item_id'";
							$query=mysqli_query($DBconnect, $sql);
							
							while($row1=mysqli_fetch_row($query))
							{
								
								$sql2="SELECT * FROM price WHERE item_id='$item_id'";
								$query2=mysqli_query($DBconnect, $sql2);
								
								while($row2=mysqli_fetch_row($query2))
								{

						?>
						
								<div class="input-group">
									<div>
									<center>
										<?php echo '<img src="data:image/jpeg;base64,'.base64_encode( $row1[2] ).'" width="170" height="170"/>'; ?>
									</center>
									</br>
									</div>
								</div>
								<div class="input-group">
									<div>
										<b>Product Name</b> : <?php echo $row1[3]; ?>
									</div>
								</div>
								<div class="input-group">
									<div>
										<b>Product Description</b> : <?php echo $row1[4]; ?>
									</div>
								</div>
								<div class="input-group">
									<div>
										<b>Cost of Goods</b> : RM <?php echo $row2[7]; ?>
									</div>
								</div>
								<div class="input-group">
									<b>Product Price</b>
									<table class="table table-condensed table-striped">
										  <tr>
										  	<th>Area</th>
										    <th>AGENT PRICE</th>
										    <th>DROPSHIP PRICE</th>
										    <th>CUSTOMER HQ</th>
										  </tr>
										  
											<tr>
												<th>Semenanjung Malaysia</th>
												<td>RM<?php $sm1=$row2[1]; echo "$sm1";?></td>
												<td>RM<?php $sm3=$row2[5]; echo "$sm3";?></td>
												<td>RM<?php $sm2=$row2[3]; echo "$sm2";?></td>  
												
											</tr>
										  
											<tr>
												<th>Sabah & Sarawak</th>
												<td>RM<?php $ss1=$row2[2]; echo "$ss1";?></td>
												<td>RM<?php $ss3=$row2[6]; echo "$ss3";?></td>
												<td>RM<?php $ss2=$row2[4]; echo "$ss2";?></td>
											</tr>
										
									
									</table>
								</div>
								<div class="input-group">
									<div>
										<b>Available Quantity</b> : <?php echo $row1[5]; ?> Units
									</div>
								</div>
								<form name="update" method="get" action="postage_status.php">
									<input type="hidden" name="item_id" value="<?php echo $item_id; ?>">
									<!-- <input type="hidden" name="current_quantity" value="<?php //echo $row1[5]; ?>"> -->
									<input type="hidden" name="freepostage" value="free">

									<!-- for setting the button based on status -->
									<?php
										$getCurrent="SELECT postage_enable FROM item WHERE item_id='$item_id'";
										$queryCurrent=mysqli_query($DBconnect, $getCurrent);
										if($resultCurrent = mysqli_fetch_object($queryCurrent)){
											$status = $resultCurrent->postage_enable;
										}

										if($status === "no"){
											$col = 'green';
											$button = 'POSTAGE FEE CALCULATION: ON';
										}else{
											$col = 'gray';
											$button = 'POSTAGE FEE CALCULATION: OFF';
										}
									?>
									
									<button type="submit" class="btn btn-block btn-lg bg-<?php echo $col;?> waves-effect"><?php echo $button;?></button>
								</form>								
								</br>

								<button type="button" class="btn btn-block btn-lg bg-green waves-effect" data-toggle="modal" data-target="#smallModal1">UPDATE QUANTITY</button>
								<!-- Small Size -->
								<div class="modal fade" id="smallModal1" tabindex="-1" role="dialog">
									<div class="modal-dialog modal-sm" role="document">
										<div class="modal-content">
											<form name="update" method="get" action="update_quantity.php">
												<input type="hidden" name="item_id" value="<?php echo $item_id; ?>">
												<input type="hidden" name="current_quantity" value="<?php echo $row1[5]; ?>">
												<div class="modal-header">
													<h4 class="modal-title" id="smallModalLabel">Update Comfirmation</h4>
												</div>
												<div class="modal-body">
																
													Add Item Quantity
													</br>
													<input type="number" name="add_quantity">
														
												</div>
												<div class="modal-footer">
													<button type="submit" class="btn btn-link waves-effect">UPDATE</button>
													<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
												</div>
											</form>
										</div>
									</div>
								</div>
								</br></br>

								<button type="button" class="btn btn-block btn-lg bg-green waves-effect" data-toggle="modal" data-target="#costOfGoods">UPDATE COST OF GOODS</button>
								<!-- Small Size -->
								<div class="modal fade" id="costOfGoods" tabindex="-1" role="dialog">
									<div class="modal-dialog modal-sm" role="document">
										<div class="modal-content">
											<form name="update" method="get" action="update_costOfGoods.php">
												<input type="hidden" name="item_id" value="<?php echo $item_id; ?>">
												<input type="hidden" name="current_quantity" value="<?php echo $row1[5]; ?>">
												<div class="modal-header">
													<h4 class="modal-title" id="smallModalLabel">Update Comfirmation</h4>
												</div>
												<div class="modal-body">
																
													Enter New Good Cost per Product
													</br>
													<input type="text" name="cost_of_goods">
														
												</div>
												<div class="modal-footer">
													<button type="submit" class="btn btn-link waves-effect">UPDATE</button>
													<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
												</div>
											</form>
										</div>
									</div>
								</div>
								</br></br>
								
								<button type="button" class="btn btn-block btn-lg bg-green waves-effect" data-toggle="modal" data-target="#smallModal2">UPDATE PRICE</button>
								<!-- Small Size -->
								<div class="modal fade" id="smallModal2" tabindex="-1" role="dialog">
									<div class="modal-dialog modal-sm" role="document">
										<div class="modal-content">
											<form name="update" method="get" action="update_price.php">
												<input type="hidden" name="item_id" value="<?php echo $item_id; ?>">
												<div class="modal-header">
													<h4 class="modal-title" id="smallModalLabel">Update Comfirmation</h4>
												</div>
												
												<!-- agent price -->
												<div class="modal-body">
																
													Agent Price (SM)
													</br>
													RM<input type="number" name="agent_sm" value="<?php $sm1=$row2[1]; echo "$sm1";?>">
														
												</div>
												<div class="modal-body">
																
													Agent Price (SS)
													</br>
													RM<input type="number" name="agent_ss" value="<?php $sm1=$row2[2]; echo "$sm1";?>">
														
												</div>
												
												<!-- admin price -->
												<div class="modal-body">
																
													Admin Price (SM)
													</br>
													RM<input type="number" name="end_sm" value="<?php $sm1=$row2[3]; echo "$sm1";?>">
														
												</div>
												<div class="modal-body">
																
													Admin Price (SS)
													</br>
													RM<input type="number" name="end_ss" value="<?php $sm1=$row2[4]; echo "$sm1";?>">
														
												</div>
												
												<!-- dropship price -->
												<div class="modal-body">
																
													Dropship Price (SM)
													</br>
													RM<input type="number" name="dropship_sm" value="<?php $sm1=$row2[5]; echo "$sm1";?>">
														
												</div>
												<div class="modal-body">
																
													Dropship Price (SS)
													</br>
													RM<input type="number" name="dropship_ss" value="<?php $sm1=$row2[6]; echo "$sm1";?>">
														
												</div>
												
												<div class="modal-footer">
													<button type="submit" class="btn btn-link waves-effect">UPDATE</button>
													<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
												</div>
											</form>
										</div>
									</div>
								</div>
								
								</br></br>
								<?php
										
									if($row1[7]=="Available")
									{
										?>
										<form method="get" action="status_item.php">
											<input type="hidden" name="item_id" value="<?php echo $item_id; ?>">
											<input type="hidden" name="status_item" value="Unavailable">
											<button class="btn btn-block btn-lg bg-red waves-effect" type="submit">REMOVE ITEM</button>
										</form>
										<?php
									}
									else if($row1[7]=="Unavailable")
									{
										?>
										<form method="get" action="status_item.php">
											<input type="hidden" name="item_id" value="<?php echo $item_id; ?>">
											<input type="hidden" name="status_item" value="Available">
											<button class="btn btn-block btn-lg bg-green waves-effect" type="submit">DISPLAY ITEM</button>
										</form>
										<?php
									}
								?>

						<?php
								}
							}
							
						?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="../plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="../plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="../plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="../plugins/raphael/raphael.min.js"></script>
    <script src="../plugins/morrisjs/morris.js"></script>

    <!-- ChartJs -->
    <script src="../plugins/chartjs/Chart.bundle.js"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="../plugins/flot-charts/jquery.flot.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.time.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="../plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Custom Js -->
    <script src="../js/admin.js"></script>
    <script src="../js/pages/index.js"></script>
	<script src="../js/pages/forms/basic-form-elements.js"></script>

    <!-- Demo Js -->
    <script src="../js/demo.js"></script>
<?php
					
		}
		mysqli_close($DBconnect);
	}
}
?>
</body>

</html>