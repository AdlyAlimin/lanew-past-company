<!DOCTYPE html>
<?php
session_start();
if($_SESSION['email']=="")
{
	header("location: admin/index.html");
}
else
{
	include("inc_db.php");
	if($DBconnect!==FALSE);
	{
		$SQLquery="SELECT * FROM user WHERE email='$_SESSION[email]'";
		$QueryResult=mysqli_query($DBconnect, $SQLquery);
		
		while($row=mysqli_fetch_array($QueryResult, MYSQLI_ASSOC))
		{
			$id=$row["no_id"];
			$_SESSION['id']=$row["no_id"];
			$nama=$row["nama"];
			$no_ic=$row["no_ic"];
			$alamat=$row["alamat"];
			$email=$row["email"];
			$no_tel=$row["no_tel"];
?>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | LANEW Management System</title>
    <!-- Favicon-->
    <link rel="icon" href="../lanew_icon.jpeg" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="../plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="../plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="../css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../css/themes/all-themes.css" rel="stylesheet" />
	
</head>

<body class="theme-amber">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">LANEW</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <?php 
					
						$SQLquery6="SELECT * FROM verify_user WHERE sponsor_id='$id'";
						$QueryResult6=mysqli_query($DBconnect, $SQLquery6);
							
						while($row6=mysqli_fetch_array($QueryResult6, MYSQLI_ASSOC))
						{
							if($row6['dp_image']=="")
							{
							?>
								<img src="../images/user.png" width="48" height="48" alt="User" />
							<?php
							}
							else
							echo '<img src="data:image/jpeg;base64,'.base64_encode( $row6['dp_image'] ).'" width="48" height="48"/>';
						}
					?>
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo "$id"; ?></div>
                    <div class="email"><?php echo "$email"; ?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="profile.php"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="view_order_list.php"><i class="material-icons">shopping_cart</i>Order List</a></li>
							<li><a href="view_withdraw_list.php"><i class="material-icons">account_balance_wallet</i>Withdraw List</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="logout.php"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
				
                    <?php include 'side_menu.php'; ?>
					
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    <img src="images/copy.jpg" alt="copy" style="width:270px;height:70px;">
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
			
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
            <div class="row clearfix js-sweetalert" >
                
				
				
                <!-- Latest Social Trends -->
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="body bg-amber">
                            <div class="font-bold m-b--35">WHATS NEW TODAY?</div>
                            <ul class="dashboard-stat-list">
                                <li>
								<?php
								
									$sql="SELECT COUNT(*) FROM order_list WHERE order_status='Pending'";
									$rs=mysqli_query($DBconnect, $sql);
									
									$result=mysqli_fetch_array($rs);
								
								?>
                                    <u>Order Notification</u>
									</br>
									You got
                                    <span><b><?php echo $result[0]; ?></b> <small>new notification</small></span>
									<span class="pull-right">
										<form method="get" action="view_order_list.php">
										<?php
										if($result[0]>0)
										{
											?>
											<button type="submit" class="btn bg-red waves-effect btn-xs" >View More</button>
											<?php
										}
										else
										{
											?>
											
											<?php
										}
										?>
										</form>
									</span>
                                </li>
								<li>
								<?php
								
								$sql1="SELECT * FROM reg_dropship WHERE id_sponsor='$id'";
								$rs1=mysqli_query($DBconnect, $sql1);
									
								$result1=mysqli_fetch_array($rs1);
								{
										
									$dropship_id=$result1["dropship_id"];
										
									$sql="SELECT COUNT(*) FROM dropship_order WHERE order_status='Pending' AND dropship_id='$dropship_id'";
									$rs=mysqli_query($DBconnect, $sql);
									
									$result=mysqli_fetch_array($rs);
								
								?>
                                    <u>Dropship Order Notification</u>
									</br>
									You got
                                    <span><b><?php echo $result[0]; ?></b> <small>new notification</small></span>
									<span class="pull-right">
										<form method="get" action="view_order_dropship.php">
										<?php
										if($result[0]>0)
										{
											?>
											<button type="submit" class="btn bg-red waves-effect btn-xs" >View More</button>
											<?php
										}
										else
										{
											?>
											
											<?php
										}
								}
										?>
										</form>
									</span>
                                </li>
                                <li>
								<?php
								
									$sql1="SELECT COUNT(*) FROM withdraw WHERE status_withdraw='Pending'";
									$rs1=mysqli_query($DBconnect, $sql1);
									
									$result1=mysqli_fetch_array($rs1);
								
								?>
                                    <u>Withdraw Notification</u>
									</br>
									You got
                                    <span><b><?php echo $result1[0]; ?></b> <small>new notification</small></span>
									<span class="pull-right">
										<form method="get" action="view_withdraw_list.php">
										<?php
											if($result1[0]>0)
											{
												?>
												<button type="submit" class="btn bg-red waves-effect btn-xs" >View More</button>
												<?php
											}
											else
											{
												?>
												
												<?php
											}
										?>
										</form>
									</span>
                                </li>
								<li>
								<?php
								
									$sql3="SELECT COUNT(*) FROM verify_user WHERE status_reg='NEW'";
									$rs3=mysqli_query($DBconnect, $sql3);
									
									$result3=mysqli_fetch_array($rs3);
								
								?>
                                    <u>New Verification Agent Notification</u>
									</br>
									You got
                                    <span><b><?php echo $result3[0]; ?></b> <small>new notification</small></span>
									<span class="pull-right">
										<form method="get" action="verify_agent_request.php">
										<?php
											if($result3[0]>0)
											{
												?>
												<button type="submit" class="btn bg-red waves-effect btn-xs" >View More</button>
												<?php
											}
											else
											{
												?>
												
												<?php
											}
										?>
										</form>
									</span>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>

                <!-- #END# Latest Social Trends -->
                <!-- Answered Tickets -->
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <?php include "top_agent_display.php"; ?>
                </div>
                <!-- #END# Answered Tickets -->
            </div>

            <div class="row clearfix">
			
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="card">
                        <div class="header">
                            <h2>Agent List</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
                                            <th>No. ID</th>
											<th>Name</th>
                                            <th>Status</th>
											<th>Action</th>
                                        </tr>
                                    </thead>
									
									<?php
			
										$SQLquery2="SELECT * FROM sponsor WHERE id_sponsor='$id' LIMIT 10";
										$QueryResult2=mysqli_query($DBconnect, $SQLquery2);
											
										while($row2=mysqli_fetch_array($QueryResult2, MYSQLI_ASSOC))
										{
											$reg_id=$row2["id_reg"];
											
											$SQLquery3="SELECT * FROM user WHERE no_id='$reg_id'";
											$QueryResult3=mysqli_query($DBconnect, $SQLquery3);
												
											while($row3=mysqli_fetch_array($QueryResult3, MYSQLI_ASSOC))
											{
												$nama1=$row3["nama"];
												$user_status=$row3["user_status"];
								
									?>
									<tbody>
                                        <tr>
                                            <td><?php echo "$reg_id"; ?></td>
											<td><?php echo "$nama1"; ?></td>
                                            <td>
											<?php 
												if($user_status=="Active")
												{
													?>
													<span class="label bg-green">Active</span>
													<?php
												}
												else if($user_status=="Suspended")
												{
													?>
													<span class="label bg-red">Suspended</span>
													<?php
												}
											?>
											</td>
											<td>
											<form method="get" action="edit_sponsor.php">
												<input type="hidden" name="user_id" value="<?php echo "$reg_id"; ?>">
												<button type="submit" class="btn bg-blue waves-effect btn-xs">Edit Profile</button>
											</form>
											</br>
											</td>
                                        </tr>
                                    </tbody>
									<?php
											}
										}
									
									?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
				
				
            </div>
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="../plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="../plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="../plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="../plugins/raphael/raphael.min.js"></script>
    <script src="../plugins/morrisjs/morris.js"></script>

    <!-- ChartJs -->
    <script src="../plugins/chartjs/Chart.bundle.js"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="../plugins/flot-charts/jquery.flot.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.time.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="../plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Custom Js -->
    <script src="../js/admin.js"></script>
    <script src="../js/pages/index.js"></script>

    <!-- Demo Js -->
    <script src="../js/demo.js"></script>
	
<?php
					
		}
		mysqli_close($DBconnect);
	}
}
?>
</body>

</html>