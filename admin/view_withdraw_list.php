<!DOCTYPE html>
	<?php
session_start();
if($_SESSION['email']=="")
{
	header("location: index.html");
}
else
{
	include("inc_db.php");
	if($DBconnect!==FALSE);
	{
		$SQLquery="SELECT * FROM user WHERE email='$_SESSION[email]'";
		$QueryResult=mysqli_query($DBconnect, $SQLquery);
		
		while($row=mysqli_fetch_array($QueryResult))
		{
			$id=$row["no_id"];
			$_SESSION['id']=$row["no_id"];
			$email=$row["email"];
?>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | LANEW Management System</title>
    <!-- Favicon-->
    <link rel="icon" href="../lanew_icon.jpeg" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="../plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="../plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="../css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-amber">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">LANEW ADMIN SITE</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <?php 
					
						$SQLquery61="SELECT * FROM verify_user WHERE sponsor_id='$id'";
						$QueryResult61=mysqliquery($DBconnect, $SQLquery61);
							
						while($row61=mysqli_fetch_array($QueryResult61))
						{
							if($row61['dp_image']=="")
							{
							?>
								<img src="../images/user.png" width="48" height="48" alt="User" />
							<?php
							}
							else
							echo '<img src="data:image/jpeg;base64,'.base64_encode( $row61['dp_image'] ).'" width="48" height="48"/>';
						}
					?>
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo "$id"; ?></div>
                    <div class="email"><?php echo "$email"; ?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="profile.php"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="view_order_list.php"><i class="material-icons">shopping_cart</i>Order List</a></li>
							<li><a href="view_withdraw_list.php"><i class="material-icons">account_balance_wallet</i>Withdraw List</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="logout.php"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
				
                    <?php include 'side_menu.php'; ?>
					
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    <img src="images/copy.jpg" alt="copy" style="width:270px;height:70px;">
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
            <div class="row clearfix">
			
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                WITHDRAW MENU
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group" id="accordion_17" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-col-pink">
                                            <div class="panel-heading" role="tab" id="headingOne_17">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion_17" href="#collapseOne_17" aria-expanded="true" aria-controls="collapseOne_17">
                                                        <i class="material-icons">payment</i> 
														WITHDRAW LIST REQUEST
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne_17" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_17">
                                                <div class="panel-body">
                                                    <div class="table-responsive">
														<table class="table table-hover dashboard-task-infos">
															<thead>
																<tr>
																	<th>Agent ID</th>
																	<th>Agent Name</th>
																	<th>Withdraw Category</th>
																	<th>Bank Account</th>
																	<th>Bank Type</th>
																	<th>Total Withdraw</th>
																	<th>Status</th>
																	<th>Action</th>
																</tr>
															</thead>
															
															<?php
									
																$SQLquery2="SELECT * FROM withdraw WHERE status_withdraw='Pending' ORDER BY withdraw_id DESC";
																$QueryResult2=mysqli_query($DBconnect, $SQLquery2);
																	
																while($row2=mysqli_fetch_array($QueryResult2))
																{
																	$sponsor_id=$row2["sponsor_id"];
																	$withdraw_id=$row2["withdraw_id"];
																	$withdraw_type=$row2["withdraw_type"];
																	$withdraw_total=$row2["withdraw_total"];
																	
																	$SQLquery3="SELECT * FROM user WHERE no_id='$sponsor_id'";
																	$QueryResult3=mysqli_query($DBconnect, $SQLquery3);
																		
																	while($row3=mysqli_fetch_array($QueryResult3))
																	{
																		$nama=$row3["nama"];
																		$bank_acc_no=$row3["bank_acc_no"];
																		$bank_type=$row3["bank_type"];
														
															?>
															<tbody>
																<tr>
																	<td><?php echo "$sponsor_id"; ?></td>
																	<td><?php echo "$nama"; ?></td>
																	<td><?php echo ucwords($withdraw_type); ?> Bonus</td>
																	<td><?php echo "$bank_acc_no"; ?></td>
																	<td><?php echo "$bank_type"; ?></td>
																	<td>RM<?php echo "$withdraw_total"; ?></td>
																	<td><span class="label bg-orange">Pending</span></td>
																	<td>
																		<button type="button" class="btn btn-danger waves-effect" data-toggle="modal" data-target="#smallModal">CONFIRMATION</button>
																		<!-- Small Size -->
																		<div class="modal fade" id="smallModal" tabindex="-1" role="dialog">
																			<div class="modal-dialog modal-sm" role="document">
																				<div class="modal-content">
																					<form name="withdraw" method="post" action="withdraw_status.php" enctype="multipart/form-data">
																						<input type="hidden" name="withdraw_id" value="<?php echo $withdraw_id; ?>">
																						<div class="modal-header">
																							<h4 class="modal-title" id="smallModalLabel">Withdraw Confirmation</h4>
																						</div>
																						<div class="modal-body">
																						
																							Withdraw Confirmation Receipt
																						
																							<div class="input-group">
																								<span class="input-group-addon">
																									<i class="material-icons">photo</i>
																								</span>
																								<div class="form-line">
																									<input type="file" class="form-control" name="image">
																								</div>
																							</div>
																						
																							Confirm Withdraw Sponsor Bonus?
																						
																						</div>
																						<div class="modal-footer">
																							<button type="submit" class="btn bg-green waves-effect">CONFIRM</button>
																							<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
																						</div>
																					</form>
																				</div>
																			</div>
																		</div>
																	</td>
																</tr>
															</tbody>
															<?php
																	}
																}
															
															?>
														</table>
													</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-col-cyan">
                                            <div class="panel-heading" role="tab" id="headingTwo_17">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_17" href="#collapseTwo_17" aria-expanded="false"
                                                       aria-controls="collapseTwo_17">
                                                        <i class="material-icons">history</i> 
														WITHDRAW LIST HISTORY
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo_17" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_17">
                                                <div class="panel-body">
                                                    <div class="table-responsive">
														<table class="table table-hover dashboard-task-infos">
															<thead>
																<tr>
																	<th>Agent ID</th>
																	<th>Agent Name</th>
																	<th>Withdraw ID</th>
																	<th>Withdraw Type</th>
																	<th>Bank Account</th>
																	<th>Bank Type</th>
																	<th>Withdraw Total</th>
																	<th>Withdraw Status</th>
																	<th>Withdraw Receipt</th>
																</tr>
															</thead>
															
															<?php
									
																$SQLquery2="SELECT * FROM withdraw ORDER BY withdraw_id DESC";
																$QueryResult2=mysqli_query($DBconnect, $SQLquery2);
																	
																while($row2=mysqli_fetch_array($QueryResult2))
																{
																	$sponsor_id=$row2["sponsor_id"];
																	$withdraw_id=$row2["withdraw_id"];
																	$withdraw_type=$row2["withdraw_type"];
																	$status_withdraw=$row2["status_withdraw"];
																	$withdraw_receipt=$row2["withdraw_receipt"];
																	$withdraw_total=$row2["withdraw_total"];
																	
																	$SQLquery4="SELECT * FROM user WHERE no_id='$sponsor_id'";
																	$QueryResult4=mysqli_query($DBconnect, $SQLquery4);
																		
																	while($row4=mysqli_fetch_array($QueryResult4))
																	{
																		$nama=$row4["nama"];
																		$bank_acc_no=$row4["bank_acc_no"];
																		$bank_type=$row4["bank_type"];
														
															?>
															<tbody>
																<tr>
																	<td><?php echo "$sponsor_id"; ?></td>
																	<td><?php echo "$nama"; ?></td>
																	<td><?php echo "$withdraw_id"; ?></td>
																	<td><?php echo ucwords($withdraw_type); ?> Bonus</td>
																	<td><?php echo "$bank_acc_no"; ?></td>
																	<td><?php echo "$bank_type"; ?></td>
																	<td>RM<?php echo "$withdraw_total"; ?></td>
																	<td>
																	<?php 
																		if($status_withdraw=="Pending")
																		{
																			?>
																			<span class="label bg-amber">Pending</span>
																			<?php
																		}
																		else if($status_withdraw=="Confirm")
																		{
																			?>
																			<span class="label bg-green">Confirm</span>
																			<?php
																		}
																	?>
																	</td>
																	<td>
																		<div>
																		<?php
																			if($withdraw_receipt==NULL)
																			{
																				?>

																				<?php
																			}
																			else
																			{
																				?>
																		<button type="button" class="btn bg-blue waves-effect btn-xs" data-toggle="modal" data-target="#smallModal1">VIEW RECEIPT</button>
																						<!-- Small Size -->
																						<div class="modal fade" id="smallModal1" tabindex="-1" role="dialog">
																							<div class="modal-dialog" role="document">
																								<div class="modal-content">
																									
																										<input type="hidden" name="withdraw_type" value="sponsor">
																										<div class="modal-header">
																											<h4 class="modal-title" id="smallModalLabel">Receipt Comfirmation</h4>
																										</div>
																										<div class="modal-body">
																										<center>
																											<?php echo '<img src="data:image/jpeg;base64,'.base64_encode( $withdraw_receipt ).'" width="250" height="350"/>'; ?>
																										</center>
																										</div>
																										<div class="modal-footer">
																											
																										</div>
																		
																								</div>
																							</div>
																						</div>
																		</div>
																</td>
																</tr>
															</tbody>
															<?php
																			}
																	}
																}
															
															?>
														</table>
													</div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			
            </div>			
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="../plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="../plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="../plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="../plugins/raphael/raphael.min.js"></script>
    <script src="../plugins/morrisjs/morris.js"></script>

    <!-- ChartJs -->
    <script src="../plugins/chartjs/Chart.bundle.js"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="../plugins/flot-charts/jquery.flot.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="../plugins/flot-charts/jquery.flot.time.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="../plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Custom Js -->
    <script src="../js/admin.js"></script>
    <script src="../js/pages/index.js"></script>

    <!-- Demo Js -->
    <script src="../js/demo.js"></script>
<?php
					
		}
		mysqli_close($DBconnect);
	}
}
?>
</body>

</html>