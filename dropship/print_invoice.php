<!doctype html>
<?php
session_start();
if($_SESSION['email']=="")
{
	header("location: index.html");
}
else
{
	include("inc_db.php");
	if($DBconnect!==FALSE);
	{
		
		$order_id=$_GET['order_id'];
		$item_name=$_GET['item_name'];
		$item_price=$_GET['item_price'];
		$order_quantity=$_GET['order_quantity'];
		
		$sql="SELECT * FROM dropship_order WHERE order_id='$order_id'";
		$query=mysql_query($sql, $DBconnect);
							
		while(($row=mysql_fetch_array($query))!==FALSE)
		{
?>
<html>
<head>
    <meta charset="utf-8">
    <title>INVOICE PURCHASE</title>
    
    
    
    	
		
	
	
	
    <style>
    .invoice-box{
        max-width:800px;
        margin:auto;
        padding:30px;
        border:1px solid #eee;
        box-shadow:0 0 10px rgba(0, 0, 0, .15);
        font-size:16px;
        line-height:24px;
        font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color:#555;
    }
    
    .invoice-box table{
        width:100%;
        line-height:inherit;
        text-align:left;
    }
    
    .invoice-box table td{
        padding:5px;
        vertical-align:top;
    }
    
    .invoice-box table tr td:nth-child(2){
        text-align:right;
    }
    
    .invoice-box table tr.top table td{
        padding-bottom:20px;
    }
    
    .invoice-box table tr.top table td.title{
        font-size:45px;
        line-height:45px;
        color:#333;
    }
    
    .invoice-box table tr.information table td{
        padding-bottom:40px;
    }
    
    .invoice-box table tr.heading td{
        background:#eee;
        border-bottom:1px solid #ddd;
        font-weight:bold;
    }
    
    .invoice-box table tr.details td{
        padding-bottom:20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom:1px solid #eee;
    }
    
    .invoice-box table tr.item.last td{
        border-bottom:none;
    }
    
    .invoice-box table tr.total td:nth-child(2){
        border-top:2px solid #eee;
        font-weight:bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td{
            width:100%;
            display:block;
            text-align:center;
        }
        
        .invoice-box table tr.information table td{
            width:100%;
            display:block;
            text-align:center;
        }
    }
    
    @media print {
  		#printPageButton {
    		display: none;
  		}
			}
    </style>
<script>
function goBack() {
    window.history.back();
}
</script>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="images/logo1.png" style="width:100%; max-width:300px;">
                            </td>
                            
                            <td>
								Invoice No. : <?php echo $row['invoice_id']; ?></br>
                                Created: <?php echo date("Y/m/d"); ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                No 33A Jalan Bukit Pandan Bistari 6 <br>
                                Bukit Pandan Bistari <br>
                                56100 Cheras <br>
                                Kuala Lumpur
                            </td>
                            
                            <td>
                                Acme Corp.<br>
                                John Doe<br>
                                john@example.com
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    Payment Method
                </td>
                
                <td>
                    Amount
                </td>
            </tr>
            
            <tr class="details">
                <td>
                    Deposit Transfer
                </td>
                
                <td>
                    RM <?php echo $row['order_total']; ?>
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    Item
                </td>
                
                <td>
                    Price
                </td>
            </tr>
            
            <tr class="item">
                <td>
                    <?php echo $item_name; ?>
                </td>
                
                <td>
                    RM <?php echo $item_price; ?> X <?php echo $order_quantity; ?>
                </td>
            </tr>
            
            <tr class="total">
                <td></td>
                
                <td>
                   Total: RM <?php echo $row['order_total']; ?>
                </td>
            </tr>
        </table>
        
        <button id="printPageButton" onClick="window.print();">Print</button> <button id="printPageButton" onclick="goBack()">Back</button>
    </div>
    
	<?php
		}
					
	}
	mysql_close($DBconnect);
}
?>



</body>
</html>
