<ul class="list">
	<li class="header">MAIN NAVIGATION</li>
	
	<li class="active">
		<a href="home.php">
			<i class="material-icons">home</i>
			<span>Home</span>
		</a>
	</li>

	<li>
		<a href="view_profile.php">
			<i class="material-icons">person</i>
			<span>Profile</span>
		</a>
	</li>
	
	<li>
		<a href="javascript:void(0);" class="menu-toggle">
			<i class="material-icons">shopping_cart</i>
				<span>MANAGE ORDER</span>
		</a>
		<ul class="ml-menu">
			<li>
				<a href="display_item.php">
					<i class="material-icons">shopping_cart</i>
					<span>Add Order</span>
				</a>
			</li>
			<li>
				<a href="view_order_list.php">
					<i class="material-icons">list</i>
					<span>Order List</span>
				</a>
			</li>
		</ul>
	</li>	

	<li>
		<a href="#">
			<i class="material-icons">help_outline</i>
			<span>HELP</span>
		</a>
	</li>
</ul>